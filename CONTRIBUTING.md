# Contributing

The following is a set of guidelines and resources for contributing to BASEmesh.

## Links and Resources

- [BASEmesh repository](https://gitlab.ethz.ch/vaw/public/basemesh-v2)
- [GitLab issue tracker](https://gitlab.ethz.ch/vaw/public/basemesh-v2/issues)
- [Wiki developer pages](https://gitlab.ethz.ch/vaw/public/basemesh-v2/-/wikis/home#developer-pages)

## Coding Style

- Follow the official Python style guides [PEP 7](https://www.python.org/dev/peps/pep-0007/) (C code) or [PEP 8](https://www.python.org/dev/peps/pep-0008/) (Python code) respectively.

    For code closely tied into the Qt or QGIS wrapper modules, it is acceptable to use camel case as this is the prevalent style used there.
- Be fully type hinted according to [PEP 484](https://www.python.org/dev/peps/pep-0484/), more on that below.
- Document functions and classes in the [Sphinx docstring format](https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html#the-sphinx-docstring-format).

    Use separate `:type <param>:` tags for any arguments and use cross-referencing syntax (e.g. `` :class:`basemesh.Mesh` ``). While Sphinx' autodoc extension does support parsing type annotations, this capability is still quite rusty and tends to produce unlinkable results.

    When referencing built-in types like `dict`, `set`, or `list`, use the Python 3.10 syntax (``:class:`dict` [K, V]``) rather than their overloads from the typing module (``:class:`typing.Dict [K, V]``). We use the typing module variants for compatibility with older Python versions, but in the docs we want to link to their modern, compact representations.
- This code is type checked with [pyright](https://github.com/microsoft/pyright) and [pylint](https://pylint.pycqa.org/en/latest/) as part of the CI pipeline. It is recommended to run these tools locally prior to pushing. On VS Code, consider the [Pylance](https://github.com/microsoft/pylance-release) extension for fast real-time checking of the source.

    When working on the QGIS plugin, use the [qgis-stubs](http://pypi.org/project/qgis-stubs) package to gain access to static type hints for the QGIS Python API.

## Documentation

Python API Documentation is built using Sphinx, with the source living in the `docs/` directory. Run the following command to generate the HTML docs:

```sh
python -m sphinx.cmd.build -b html . _build
```

The Sphinx version, extensions and other dependencies required are listed in `docs/requirements.txt`.

Plugin-specific or user-facing documentation such as tutorials or case studies are documented in the GitLab Wiki, not in the repository itself.

## Testing

This project uses the [`unittest` testing framework](https://docs.python.org/3/library/unittest.html):

```sh
python -m unittest discover -p "*_test.py" ./tests/
```
