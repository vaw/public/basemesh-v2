"""Convert a multiline Docstring from Python to C.

This takes in a single argument, the name of a plain text file
containing one or more multiline Python strings (including triple
quotes).

It then breaks this string up, preserving indentation and outputting
a new file of name `{basename}_converted.{ext}`, which contains the
same docstrings and indentation, but in a C extension friendly format.

Note that this is a development tool and therefore not very robust.
Single-line docstrings won't work, weirdly formatted docstrings won't
work, multiple docstrings per line won't work.
"""

import os
import sys
from typing import List


def convert_docstrings(input_: str, output: str) -> None:
    """Process and convert the given file.

    Process every docstring in the input file and save it to the
    output path.
    """
    # Create a list of all input docstrings
    docstrings: List[str] = []
    current = ''
    is_doc = False
    with open(input_, encoding='utf-8') as input_file:
        for line in input_file:
            # Docstring start/end
            if line.lstrip().startswith('"""'):

                # If there is an ongoing docstring being parsed, this is the end
                if current:
                    docstrings.append(current.rstrip())
                    current = ''
                    is_doc = False

                # Otherwise, start a new docstring
                else:
                    current = line.replace('"""', '')
                    is_doc = True

            # Regular lines get added to the "current" tally if they're in a
            # docstring
            elif is_doc:
                current += line

    # Split these docstrings at their newlines
    docstrings_out = [s.split('\n') for s in docstrings]

    with open(output, 'w', encoding='utf-8') as output_file:
        for doc in docstrings_out:
            for line in doc:
                output_file.write(f'"{line}\\n"\n')
            output_file.write('\n\n')


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Exactly one argument allowed: filepath')
        sys.exit(1)

    input_path = sys.argv[1]
    basename, ext = os.path.splitext(input_path)
    output_path = f'{basename}_converted{ext}'
    convert_docstrings(input_path, output_path)
