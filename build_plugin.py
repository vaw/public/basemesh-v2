"""Generation utility for the BASEmesh QGIS plugin.

This script can be thought of as the QGIS plugin equivalent of the
``setup.py`` script used to build the BASEmesh Python package.

This script requires Git to be installed and on PATH.
"""

import argparse
import configparser
import json
import os
import pathlib
import shutil
import subprocess
import sys
from typing import Any, Dict, List

try:
    import markdown2  # type: ignore
except ImportError as err:
    raise ImportError(
        'Building the BASEmesh plugin requires the markdown2 package:\n'
        'python -m pip install markdown2') from err

# Internal name of the plugin (used for folder name)
# NOTE: Due to the way plugins are loaded by QGIS, this name may not collide
# with the underlying "basemesh" Python package.
PLUGIN_NAME = 'basemesh_v2'

REPO_PATH = pathlib.Path(__file__).parent
PLUGIN_PATH = REPO_PATH / 'qgis_plugin'


def build_plugin(target_dir: pathlib.Path) -> None:
    commit = get_repository_hash()
    print(f'Building plugin at commit {commit} to {target_dir}')

    # Copy the contents of the plugin directory to the build directory
    _copy_recursive(PLUGIN_PATH, target_dir)

    if os.name == 'nt':
        # Compile GUI
        compile_gui(target_dir)
        # Compile QRC resource file
        compile_resources(target_dir)
    else:
        print('Skipping GUI compilation on non-Windows platform as PyQt5 does '
              'not ship with a resource compiler on Linux.')
    # Generate HTML from Markdown
    generate_html(target_dir)

    # Clean up the build directory
    (target_dir / 'resources.qrc').unlink()
    _unlink_recursive(target_dir / 'templates')

    # Copy basemesh module
    module_path = target_dir / 'libs' / 'basemesh'
    module_path.mkdir(parents=True, exist_ok=True)
    _copy_recursive(REPO_PATH / 'basemesh', module_path)

    # TODO: Extract built extensions for bundling
    extract_built_extensions()

    # Load project configuration
    config = read_jsonc(PLUGIN_PATH / 'config.jsonc')
    # Load version info
    version = extract_version(PLUGIN_PATH / '__init__.py')
    config['version'] = version
    # Replace maintainer-friendly entries with QGIS fields
    config['tags'] = ','.join(config['tags'])
    config['qgisMinimumVersion'] = config.pop('qgisMinSupported')
    config['qgisMaximumVersion'] = config.pop('qgisMaxSupported')
    config['tracker'] = config.pop('issueTracker')
    config['repository'] = config.pop('pluginRepo')

    # Generate metadata.txt
    metadata = configparser.ConfigParser()
    # Preserve case of keys
    metadata.optionxform = str  # type: ignore
    metadata['general'] = config
    with open(target_dir / 'metadata.txt', 'w', encoding='utf-8') as file_:
        metadata.write(file_, space_around_delimiters=False)
        file_.write(f'; Plugin generated from commit {commit}\n')
    # Remove config.jsonc
    (target_dir / 'config.jsonc').unlink()


def get_qgis_plugins_dir() -> pathlib.Path:
    """Get the QGIS plugins folder for the current platform.

    If the QGIS plugins directory does not exist yet (e.g. because
    QGIS was only just installed), it will be created automatically.
    """
    # Find QGIS user data directory
    user_data: pathlib.Path
    # Windows
    if os.name == 'nt':
        user_data = pathlib.Path(os.environ['APPDATA'])
    # Linux
    elif os.name == 'posix':
        user_data = pathlib.Path(os.environ['HOME']) / '.local' / 'share'
    # Other (unsupported)
    else:
        raise NotImplementedError(
            f'Unsupported OS for live deployment: {os.name}')
    # Find the plugins directory
    plugins_dir = (user_data / 'QGIS' / 'QGIS3' / 'profiles'
                   / 'default' / 'python' / 'plugins')
    if not plugins_dir.is_dir():
        plugins_dir.mkdir(parents=True)
    return plugins_dir


def get_repository_hash() -> str:
    """Return the short hash for the current git HEAD.

    This is written into the plugin to assist with troubleshooting as
    plugin releases may not coincide with package versions.
    """
    cmd = ['git', 'rev-parse', '--short', 'HEAD']
    return subprocess.check_output(cmd).decode('ascii').strip()


def find_resource_compiler(verbose: bool = False) -> pathlib.Path:
    """Locate the "pyrcc5" binary shipped with Qt5.

    This resource compiler is used to compile external resources into a
    format that Qt can use at runtime.

    :param verbose: If true, the paths to the library and resource
        compiler will be printed to STDOUT.
    :type verbose: bool
    :returns: The path to the resource compiler.
    """
    # Locate the installation directory of Qt
    err_msg = ('Unable to locate Qt5 installation directory. Please make '
               'sure Qt5 is installed on the Python interpreter running this '
               'script.')
    try:
        import PyQt5
    except ImportError as err:
        raise ImportError(err_msg) from err
    if PyQt5.__file__ is None:
        raise ImportError(err_msg)
    qt_module = pathlib.Path(PyQt5.__file__).parent
    if not qt_module.is_dir():
        raise FileNotFoundError(err_msg)
    if verbose:
        print(f'Found Qt5 installation at {qt_module}')
    # Locate the Qt resource compiler
    rrc_path = qt_module.parents[1] / 'Scripts' / 'pyrcc5.exe'
    if not rrc_path.is_file():
        raise FileNotFoundError(
            f'Unable to locate Qt resource compiler, expected at {rrc_path}')
    # Locate the resource file
    if verbose:
        print(f'Using Qt5 resource compiler at {rrc_path}')
    return rrc_path


def find_ui_compiler(verbose: bool = False) -> pathlib.Path:
    """Locate the "pyuic5" binary shipped with Qt5.

    This UI compiler is used to compile Qt Designer .ui files into
    Python code.

    :param verbose: If true, the paths to the library and UI compiler
        will be printed to STDOUT.
    :type verbose: bool
    :returns: The path to the UI compiler.
    """
    # Locate the installation directory of Qt
    err_msg = ('Unable to locate Qt5 installation directory. Please make '
               'sure Qt5 is installed on the Python interpreter running this '
               'script.')
    try:
        import PyQt5
    except ImportError as err:
        raise ImportError(err_msg) from err
    if PyQt5.__file__ is None:
        raise ImportError(err_msg)
    qt_module = pathlib.Path(PyQt5.__file__).parent
    if not qt_module.is_dir():
        raise FileNotFoundError(err_msg)
    if verbose:
        print(f'Found Qt5 installation at {qt_module}')
    # Locate the Qt UI compiler
    uic_path = qt_module.parents[1] / 'Scripts' / 'pyuic5.exe'
    if not uic_path.is_file():
        raise FileNotFoundError(
            f'Unable to locate Qt UI compiler, expected at {uic_path}')
    # Locate the resource file
    if verbose:
        print(f'Using Qt5 UI compiler at {uic_path}')
    return uic_path


def compile_gui(target_dir: pathlib.Path) -> None:
    """Compile the plugin GUI from the Qt Designer .UI files."""
    uic_path = find_ui_compiler()
    # Compile each plugin .ui file
    templates = PLUGIN_PATH / 'templates'
    for filename in templates.iterdir():
        if filename.suffix == '.ui':
            subprocess.check_call([
                str(uic_path),  # Executable
                str(filename),  # Input file
                '-o', str(target_dir / f'gui/{filename.stem}.py'),  # Output
            ])


def compile_resources(target_dir: pathlib.Path) -> None:
    """Bundle the Qt resource file (``resources.qrc``) into a binary.

    This binary is required for Qt to embed external icons into the
    plugin GUI.
    """
    resource_file = PLUGIN_PATH / 'resources.qrc'
    if not resource_file.is_file():
        raise FileNotFoundError(
            f'Unable to locate resource file: {resource_file}')
    rrc_path = find_resource_compiler()
    subprocess.check_call([
        str(rrc_path),  # Executable
        '-o', str(target_dir / 'resources_rc.py'),  # Output path
        str(resource_file),  # Input path
    ])


def generate_html(path: pathlib.Path) -> None:
    """Generate HTML source files from Markdown input."""
    # Process all .md files
    templates = PLUGIN_PATH / 'templates'
    for filename in templates.iterdir():
        if filename.suffix == '.md':
            html: str = markdown2.markdown_path(filename)  # type: ignore
            path.joinpath(f'gui/{filename.stem}.html').write_text(html)


def read_jsonc(path: pathlib.Path) -> Dict[str, Any]:
    """Read a JSONC file using C-style comments.

    The native json module does not support comments. This function
    first strips any comments before parsing the file via json.loads().

    Only line comments are supported, no block comments or comments at
    the end of a line.
    """
    # Strip comments
    lines: List[str] = []
    with path.open('r', encoding='utf-8') as file_:
        for line in file_:
            line = line.strip()
            if not line.startswith('//'):
                lines.append(line)
    # Parse JSON
    return json.loads('\n'.join(lines))


def extract_version(path: pathlib.Path) -> str:
    """Extract the ``__version__`` tag from a Python file.

    The plugin cannot be loaded without a qgis dummy package, so this
    helper function instead reads the file and returns the contents of
    the first ``__version__`` tag found.

    If no version tag is found, an error is raised.
    """
    with path.open('r', encoding='utf-8') as file_:
        for line in file_:
            if line.startswith('__version__'):
                return line.split('=', maxsplit=1)[1].strip().strip('\'')
    raise ValueError(f'Unable to find version tag in {path}')


def extract_built_extensions() -> None:
    """Check the build output for built extensions and extract them.

    This allows bundling the C extensions into the plugin source. This
    is significantly less space-consuming than building separate wheels
    for each platform the plugin should run on and shipping them
    alongside the plugin.
    """
    # NOTE: Another option would be to fetch the built wheels from PyPI upon
    # installation, though this would tie the plugin to the release versions of
    # of the BASEmesh package.


def _unlink_recursive(path: pathlib.Path) -> None:
    """Unlink all files and directories in the given path."""
    for entry in path.iterdir():
        if entry.is_dir():
            _unlink_recursive(entry)
        else:
            entry.unlink()
    path.rmdir()


def _copy_recursive(src: pathlib.Path, dst: pathlib.Path) -> None:
    """Copy all files in the ``src`` directory to ``dst``.

    ``__pycache__`` directories will not be copied.

    :param src: The source directory.
    :type src: pathlib.Path
    :param dst: The destination directory.
    :type dst: pathlib.Path
    """
    for entry in src.iterdir():
        if entry.is_dir():
            if entry.name != '__pycache__':
                dst_entry = dst / entry.name
                dst_entry.mkdir(parents=True)
                _copy_recursive(entry, dst_entry)
        else:
            shutil.copy(entry, dst)


def main(deployment: str) -> None:
    """Main entry point for the script."""

    # Select target directory based on deployment type
    target_dir: pathlib.Path
    if deployment == 'repo':
        target_dir = REPO_PATH / 'build' / 'plugin' / PLUGIN_NAME
    elif deployment == 'live':
        target_dir = get_qgis_plugins_dir() / PLUGIN_NAME
    else:
        raise ValueError(f'Unknown deployment target: {deployment}')

    # Create target directory
    if target_dir.exists():
        try:
            _unlink_recursive(target_dir)
        except PermissionError as err:
            print(f'Unable to clear target directory: {target_dir}\n'
                  f'  Error message: {err}\n'
                  '  Consider running this script with elevated privileges.')
            sys.exit(1)
    target_dir.mkdir(parents=True)

    build_plugin(target_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--deployment', '-D', choices=['repo', 'live'],
                        default='repo', help='Whether to deploy into the '
                        'repository\'s build directory (repo), or directly '
                        'into the QGIS plugins directory (live).')
    args = parser.parse_args()
    main(args.deployment)
