# Triangle Website Files

These are example files provided as part of the Triangle documentation.

Note that these do not necessarily add up; triangulating "face.poly" does not yield the contents of the file "face.1.poly".
