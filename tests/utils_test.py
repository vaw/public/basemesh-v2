"""Test cases for the utils module and its functions."""

import shutil
import typing

import py2dm
from basemesh import Mesh
from basemesh.errors import BasemeshWarning
from basemesh.utils import (Matids, inherit_2dm_data, inject_matid,
                            renumber_mesh)

from test_utils import TempFileTestCase, test_data_path

_Matids = typing.Tuple[typing.Union[int, float], ...]

class UtilsTests(TempFileTestCase):
    """Tests for the utils module."""

    def test_renumber_mesh(self) -> None:
        # Copy the test file to a temporary directory for processing
        src = test_data_path('2dm', 'basic_unordered.2dm')
        shutil.copy(str(src), self.temp_dir.name)
        input_ = self.file(src.name)

        # Ensure the file is not already numbered
        with self.assertRaises(py2dm.errors.FormatError):
            Mesh.open(input_)

        # Renumber the mesh
        renumber_mesh(input_)
        output = input_.with_name(f'{input_.stem}_converted.2dm')
        _ = Mesh.open(output)

    def test_inherit_2dm_data(self) -> None:
        source = Mesh.open(test_data_path('2dm', 'edittest_source.2dm'))
        with self.assertWarns(BasemeshWarning):
            edited = Mesh.open(test_data_path('2dm', 'edittest_edited.2dm'))
        with self.assertWarns(BasemeshWarning):
            output = inherit_2dm_data(source, edited)
        
        self.assertEqual(len(output.nodes), len(edited.nodes))
        self.assertEqual(len(output.elements), len(edited.elements))
        self.assertEqual(len(output.node_strings), len(source.node_strings))
        
        # Ensure all nodes are present and unchanged
        for node in output.nodes:
            edited_node = edited.get_node_by_id(node.id)
            self.assertEqual(node.id, edited_node.id)
            self.assertEqual(node.pos, edited_node.pos)
        # Ensure all elements are present and using updated materials
        for element in output.elements:
            edited_element = edited.get_element_by_id(element.id)
            self.assertEqual(element.id, edited_element.id)
            self.assertEqual([n.id for n in element.nodes],
                             [n.id for n in edited_element.nodes])
            try:
                source_ele = source.get_element_by_id(element.id)
            except KeyError:
                # One element has no equivalent in the source mesh, this is
                # expected
                pass
            else:
                self.assertEqual(element.materials, source_ele.materials)
        # Ensure the first node string is unchanged
        self.assertEqual([n.id for n in output.node_strings['A']],
                         [n.id for n in source.node_strings['A']])

    def test_matid_inject_default(self) -> None:
        """Test the default function of the MATID injector."""
        source = Mesh.open(test_data_path('2dm', 'matid_inject_a.2dm'))
        other = Mesh.open(test_data_path('2dm', 'matid_inject_b.2dm'))
        output = inject_matid(source, other)

        element = output.get_element_by_id(1)
        self.assertTupleEqual(element.materials, (1.1, 0, 1, 2.1, 0, 2, 3))
        element = output.get_element_by_id(3)
        self.assertTupleEqual(element.materials, (1.3, 0, 1, 2.3, 0, 2, 3))

    def test_matid_inject_custom_replace(self) -> None:
        """Custom query replacing first MATID only."""

        def query(a: Matids, b: Matids) -> Matids:
            return b[:1] + a[1:]

        source = Mesh.open(test_data_path('2dm', 'matid_inject_a.2dm'))
        other = Mesh.open(test_data_path('2dm', 'matid_inject_b.2dm'))
        output = inject_matid(source, other, query=query)

        element = output.get_element_by_id(1)
        self.assertTupleEqual(element.materials, (2.1, 0, 1))
        element = output.get_element_by_id(3)
        self.assertTupleEqual(element.materials, (2.3, 0, 1))

    def test_matid_inject_custom_append(self) -> None:
        """Custom query appending the first MATID only."""

        def query(a: Matids, b: Matids) -> Matids:
            return a + b[:1]

        source = Mesh.open(test_data_path('2dm', 'matid_inject_a.2dm'))
        other = Mesh.open(test_data_path('2dm', 'matid_inject_b.2dm'))
        output = inject_matid(source, other, query=query)

        element = output.get_element_by_id(1)
        self.assertTupleEqual(element.materials, (1.1, 0, 1, 2.1))
        element = output.get_element_by_id(3)
        self.assertTupleEqual(element.materials, (1.3, 0, 1, 2.3))
