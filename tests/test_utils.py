"""Shared utilities for BASEmesh test cases."""

import pathlib
import tempfile
import unittest

_TEST_DATA = pathlib.Path(__file__).parent / 'data'


class TempFileTestCase(unittest.TestCase):
    """Custom test case subclass with temporary file support."""

    temp_dir: tempfile.TemporaryDirectory[str]

    def file(self, filename: str) -> pathlib.Path:
        return pathlib.Path(self.temp_dir.name) / filename

    def setUp(self) -> None:
        self.temp_dir = tempfile.TemporaryDirectory()
        _ = self.temp_dir.__enter__()
        return super().setUp()

    def tearDown(self) -> None:
        self.temp_dir.__exit__(None, None, None)
        del self.temp_dir
        return super().tearDown()


def test_data_path(path: str, *args: str) -> pathlib.Path:
    """Return the path to a file in the test data directory."""
    return _TEST_DATA.joinpath(path, *args)
