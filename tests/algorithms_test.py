"""Test cases for the `algorithms` module."""

import math
import unittest

from basemesh import _algorithms as algorithms
from basemesh.types import Line2D


class TestDist2D(unittest.TestCase):
    """Tests for the `dist_2d` algorithm."""

    def test_basic(self) -> None:
        """Test basic pairs of points."""
        dist_2d = algorithms.dist_2d

        test_points = [
            ((0, 0), (0, 0), 0),
            ((0, 1), (1, 0), math.sqrt(2))
        ]

        for pt_a, pt_b, result in test_points:
            self.assertAlmostEqual(dist_2d(pt_a, pt_b), result,
                                   msg=f'{pt_a} to {pt_b}')


class TestDist3D(unittest.TestCase):
    """Tests for the `dist_3d` algorithm."""

    def test_basic(self) -> None:
        """Test basic pairs of points."""
        dist_3d = algorithms.dist_3d

        test_points = [
            ((0, 0, 0), (0, 0, 0), 0),
            ((0, 1, 0), (1, 0, 1), math.sqrt(3)),
            ((0, 0, 0), (0, 0, 2), 2)
        ]

        for pt_a, pt_b, result in test_points:
            self.assertAlmostEqual(dist_3d(pt_a, pt_b), result,
                                   msg=f'{pt_a} to {pt_b}')


class TestDistanceToPolygon(unittest.TestCase):
    """Tests for the `distance_to_polygon` algorithm."""

    def test_triangle(self) -> None:
        """Test using the edges and vertices of a triangle."""
        distance = algorithms.distance_to_polygon
        triangle = ((-1, -1), (2, 0), (0, 2))

        self.assertAlmostEqual(distance((0, 2), triangle), 0,
                               msg='Point on vertex #3')
        self.assertAlmostEqual(distance((1.2, 0.8), triangle), 0,
                               msg='Point on edge #2')
        with self.assertRaises(ValueError):
            _ = distance((0, 1), ((0, 1), (1, 2)))

    def test_closest_internal(self) -> None:
        """Test the closest-approach for internal points."""
        distance = algorithms.distance_to_polygon
        triangle = ((-1, -1), (2, 0), (0, 2))

        self.assertAlmostEqual(distance((0, 0), triangle), -math.sqrt(0.4),
                               msg='Internal point (origin)')
        self.assertAlmostEqual(distance((0, 1), triangle), -math.sqrt(0.1),
                               msg='Internal point (edge #1)')

    def test_closest_outside(self) -> None:
        """Test the closest-approach for external points."""
        distance = algorithms.distance_to_polygon
        triangle = ((-1, -1), (2, 0), (0, 2))

        self.assertAlmostEqual(distance((2, 2), triangle), math.sqrt(2),
                               msg='External point (edge)')
        self.assertAlmostEqual(distance((-1, 3), triangle), math.sqrt(2),
                               msg='External point (vertex)')
        self.assertAlmostEqual(distance((-1, -2+1e-10), triangle), 1,
                               msg='External point (floating point error)')


class TestGetIntersections(unittest.TestCase):
    """Tests for the `get_intersections` algorithm."""

    def test_square(self) -> None:
        """Basic tests using a square polygon."""
        intersections = algorithms.get_intersections

        square = (-2, -2), (2, -2), (2, 2), (-2, 2)
        line_a = (-5, -1), (5, -1)
        line_b = (-5, -3), (5, 3)

        intersec_a = {(-2, -1), (2, -1)}
        intersec_b = {(-2, -1.2), (2, 1.2)}

        self.assertSetEqual(set(intersections(line_a, square)), intersec_a,
                            msg='Square (line A)')
        self.assertSetEqual(set(intersections(line_b, square)), intersec_b,
                            msg='Square (line B)')
        with self.assertRaises(ValueError):
            _ = intersections(line_a, line_b)

    def test_partial(self) -> None:
        """Tests using partial intersections.

        A partial intersection is one where only one end of the line
        lies within the polygon.
        """
        intersections = algorithms.get_intersections

        square = (-2, -2), (2, -2), (2, 2), (-2, 2)
        line = (-5, -1), (0, -1)

        intersec = {(-2, -1)}

        self.assertSetEqual(set(intersections(line, square)), intersec,
                            msg='Partial intersection')

    def test_concave(self) -> None:
        """Tests using a concave polygon."""
        intersections = algorithms.get_intersections

        # NOTE: Sketch of what this polygon looks like:
        # +-----------+     +---+
        # |            \   /    |
        # |              +      |
        # +---------------------+
        polygon = (0, 0), (6, 0), (6, 2), (5, 2), (4, 1), (3, 2), (0, 2)

        line_a = (0, 3), (4, -1)
        line_b = (2, 1.5), (7, 1.5)

        intersec_a = {(1, 2), (3, 0)}
        intersec_b = {(3.5, 1.5), (4.5, 1.5), (6, 1.5)}

        self.assertSetEqual(set(intersections(line_a, polygon)), intersec_a,
                            msg='Concave (cut through)')
        self.assertSetEqual(set(intersections(line_b, polygon)), intersec_b,
                            msg='Concave (partial)')


class TestHalfPlaneDistance(unittest.TestCase):
    """Tests for the `half_plane_distance` algorithm."""

    def test_exact(self) -> None:
        """Basic tests expecting exact results."""
        hp_dist = algorithms.half_plane_distance

        line_on_x = (0, 0), (0, 1)
        line_on_y = (0, 0), (1, 0)

        line_parallel_x = (0, 10), (10, 10)
        line_parallel_y = (10, 0), (10, 10)

        self.assertEqual(hp_dist(line_on_x, (0, 0)), 0,
                         'Point on vertex (x)')
        self.assertEqual(hp_dist(line_on_y, (0, 0)), 0,
                         'Point on vertex (y)')
        self.assertEqual(hp_dist(line_parallel_x, (0, 0)), 10,
                         'Point parallel x axis')
        self.assertEqual(hp_dist(line_parallel_y, (0, 0)), -10,
                         'Point parallel y axis')

    def test_sign(self) -> None:
        """Test sign correctness."""
        hp_dist = algorithms.half_plane_distance

        line_para_a = (0, 5), (-5, 5)
        line_para_b = (5, 5), (5, 0)

        line_diag_a = (0, 0), (1, 1)
        line_diag_b = (0, 0), (1, -1)

        self.assertLess(hp_dist(line_para_a, (0, 0)), 0,
                        'Right-to-left (below)')
        self.assertGreater(hp_dist(line_para_a, (0, 10)), 0,
                           'Right-to-left (above)')

        self.assertGreater(hp_dist(line_para_b, (-10, 0)), 0,
                           'Top-to-bottom (left)')
        self.assertLess(hp_dist(line_para_b, (10, 0)), 0,
                        'Top-to-bottom (right)')

        self.assertLess(hp_dist(line_diag_a, (-5, 5)), 0,
                        'Bottom-left-to-top-right (top-left)')
        self.assertGreater(hp_dist(line_diag_a, (5, -5)), 0,
                           'Bottom-left-to-top-right (bottom-right)')

        self.assertGreater(hp_dist(line_diag_b, (-5, -5)), 0,
                           'Top-left-to-bottom-right (bottom-left)')
        self.assertLess(hp_dist(line_diag_b, (5, 5)), 0,
                        'Bottom-left-to-top-right (top-right)')

    def test_triangle_edges(self) -> None:
        """Test edge error for triangles."""
        hp_dist = algorithms.half_plane_distance

        triangle_basic = ((-1, 0), (1, 0), (0, 2))
        triangle_pointy = ((0, 0), (500, 10), (0, 1))

        line: Line2D = (triangle_basic[0], triangle_basic[1])
        self.assertAlmostEqual(hp_dist(line, (0, 0)), 0,
                               msg='Basic (edge #1)')
        line = triangle_basic[1], triangle_basic[2]
        self.assertAlmostEqual(hp_dist(line, (0.5, 1)), 0,
                               msg='Basic (edge #3)')
        line = triangle_pointy[0], triangle_pointy[1]
        self.assertAlmostEqual(hp_dist(line, (250, 5)), 0,
                               msg='Pointy (edge #1)')
        line = triangle_pointy[1], triangle_pointy[2]
        self.assertAlmostEqual(hp_dist(line, (250, 5.5)), 0,
                               msg='Pointy (edge #3)')


class TestInterpolateLine(unittest.TestCase):
    """Tests for the `interpolate_line` algorithm."""

    def test_basic(self) -> None:
        """Basic tests for linear interpolation."""
        interpol = algorithms.interpolate_line

        line_a = (0, 0, 0), (10, 0, 10)
        line_b = (10, 0, -10), (0, 0, 10)
        line_c = (4, 2, 1e8), (-2, 4, -1e8)

        self.assertAlmostEqual(interpol((5, 0), line_a), 5,
                               msg='Line A, Point #1')
        self.assertAlmostEqual(interpol((5, 1), line_a), 5,
                               msg='Line A, Point #2')
        self.assertAlmostEqual(interpol((-1, 2), line_b), 12,
                               msg='Line B, Point #1')
        self.assertAlmostEqual(interpol((20, 0), line_b), -30,
                               msg='Line B, Point #2')
        self.assertAlmostEqual(interpol((0, 2), line_c), -2e7,
                               msg='Line C, Point #1')
        self.assertAlmostEqual(interpol((5, 5), line_c), 1e8,
                               msg='Line C, Point #2')
        self.assertAlmostEqual(interpol((-1, 1), line_c), -4e7,
                               msg='Line C, Point #3')


class TestInterpolateTriangle(unittest.TestCase):
    """Tests for the `interpolate_triangle` algorithm."""

    def test_boundary(self) -> None:
        """Test the boundary edges and vertices of the triangle."""
        interpol = algorithms.interpolate_triangle
        triangle = ((-1, -1, 1), (1, -1, 2), (0, 2, 3))

        for index, vertex in enumerate(triangle):
            point = vertex[0], vertex[1]
            value = vertex[2]
            self.assertAlmostEqual(interpol(point, triangle), value,
                                   msg=f'Point on vertex #{index+1}')

        self.assertAlmostEqual(interpol((0, -1), triangle), 1.5,
                               msg='Point on edge #1 (50%)')
        self.assertAlmostEqual(interpol((0.5, 0.5), triangle), 2.5,
                               msg='Point on edge #2 (50%)')
        self.assertAlmostEqual(interpol((-0.5, 0.5), triangle), 2,
                               msg='Point on edge #3 (50%)')

    def test_internal(self) -> None:
        """Test internal triangle points."""
        interpol = algorithms.interpolate_triangle
        triangle = ((-1, -1, 1), (1, -1, 2), (0, 2, 3))

        self.assertAlmostEqual(interpol((-1, -0.5), triangle), 1.25,
                               msg='Point (-1.0, -0.5), expected 1.25')
        self.assertAlmostEqual(interpol((1, -0.5), triangle), 2.25,
                               msg='Point (1.0, -0.5), expected 2.25')
        self.assertAlmostEqual(interpol((-0.5, -0.5), triangle), 1.5,
                               msg='Point (-0.5, -0.5), expected 1.5')
        self.assertAlmostEqual(interpol((0.5, 0.5), triangle), 2.5,
                               msg='Point (0.5, 0.5), expected 2.5')
        self.assertAlmostEqual(interpol((0, 1), triangle), 2.5,
                               msg='Point (0.0, -1.0), expected 2.5')


class TestLineIntersection(unittest.TestCase):
    """Tests for the `line_intersection` algorithm."""

    def test_basic(self) -> None:
        """Basic tests using two lines."""
        intersect = algorithms.line_intersection
        line_a = (0, 0), (5, 0)
        line_a_rev = (5, 0), (0, 0)
        line_b = (0, -2), (5, 2)
        line_c = (0, 0), (2, 0)
        line_d = (0, 1), (5, 1)

        point_ab = (2.5, 0)

        self.assertEqual(intersect(line_a, line_b), point_ab,
                         'Regular intersection')
        _ = intersect(line_a, line_c)
        _ = intersect(line_a_rev, line_c)
        with self.assertRaises(ValueError,
                               msg='Parallel lines'):
            _ = intersect(line_a, line_d)


class TestLineSegmentsIntersection(unittest.TestCase):
    """Tests for the `line_segments_intersection` algorithm."""

    def test_basic(self) -> None:
        """Basic tests using two lines."""
        intersect = algorithms.line_segments_intersection
        line_a = (0, 0), (5, 0)
        line_b = (0, -2), (5, 2)
        line_c = (0, 0), (2, 0)
        line_d = (0, 1), (5, 1)
        line_e = (2, 2), (2, 1)
        line_f = (2, 6), (-2, 6)
        line_g = (0, -2), (0, 2)

        self.assertTrue(intersect(line_a, line_b, True),
                        'Regular intersection')
        self.assertTrue(intersect(line_a, line_c, True),
                        'Coincident lines (allowed)')
        self.assertFalse(intersect(line_a, line_c, False),
                         'Coincident lines (disallowed)')
        self.assertFalse(intersect(line_a, line_d),
                         'Parallel lines')
        self.assertFalse(intersect(line_a, line_e),
                         'No intersection (first line stops early)')
        self.assertFalse(intersect(line_a, line_f),
                         'No intersection (second line stops early)')
        self.assertTrue(intersect(line_g, line_a, True),
                        'No intersection (collinear)')


class TestPointInPolygonConcave(unittest.TestCase):
    """Tests for the `point_in_polygon_convex` algorithm."""

    def test_triangle_basic(self) -> None:
        """Basic triangle containment checks."""
        contains = algorithms.point_in_polygon_concave
        triangle = ((0, 0), (2, 0), (1, 2))

        self.assertTrue(contains((1, 1), triangle),
                        'Point in triangle')
        self.assertTrue(contains((0, 0), triangle),
                        'Point on vertex #1')
        self.assertTrue(contains((0.5, 1), triangle),
                        'Point on edge #3')
        self.assertFalse(contains((-2, 0), triangle),
                         'Point beyond edge #1')
        self.assertFalse(contains((2, 1), triangle),
                         'Point beyond edge #2')
        self.assertFalse(contains((0, 1), triangle),
                         'Point beyond edge #3')

    def test_triangle_degenerate(self) -> None:
        """Test using degenerate triangles."""
        contains = algorithms.point_in_polygon_concave
        line = ((0, 0), (1, 1))
        degenerate_triangle = ((0, 0), (1, 1), (2, 2))

        with self.assertRaises(ValueError, msg='Input is line'):
            _ = contains((0.5, 0.5), line)

        # NOTE: While it is debatable whether the following should be a valid
        # triangle, it is not up to this function to determine that.
        # Since point_in_polygon_convex also covers edges, this should still
        # succeed for points on the edges, even though it has no area.
        self.assertTrue(contains((0.5, 0.5), degenerate_triangle),
                        'Point on edges #1 and #3')
        self.assertFalse(contains((0, 2), degenerate_triangle),
                         'Point beyond edges #1 and #2')

    def test_triangle_sizes(self) -> None:
        """Test using extreme triangle sizes."""
        contains = algorithms.point_in_polygon_concave
        triangle_tiny = ((0, 0), (1e-6, 1e-6), (0.5e-6, 3e-6))
        triangle_huge = ((-2e12, -1e12), (2e12, 0), (-1e12, 3e12))

        self.assertTrue(contains((0.5e-6, 0.5e-6), triangle_tiny),
                        'Point in triangle')
        self.assertFalse(contains((-1e-6, 0), triangle_tiny),
                         'Point beyond edge #3')
        self.assertTrue(contains((0, 0), triangle_huge),
                        'Point in triangle')
        self.assertFalse(contains((-2e12, 0), triangle_huge),
                         'Point beyond edge #1')

    def test_concave_polygon(self) -> None:
        """Test using an irregular, concave polygon."""
        contains = algorithms.point_in_polygon_concave
        polygon = (0, 0), (10, 0), (10, 4), (8, 4), (7, 2), (6, 4), (0, 4)

        self.assertTrue(contains((0, 0), polygon),
                        'Point on vertex #1')
        self.assertTrue(contains((5, 0), polygon),
                        'Point on edge #1')
        self.assertFalse(contains((5, -2), polygon),
                         'Point in extension of edge #4')
        self.assertFalse(contains((7, 3), polygon),
                         'Point in concavity')


class TestPointInPolygonConvex(unittest.TestCase):
    """Tests for the `point_in_polygon_convex` algorithm."""

    def test_triangle_basic(self) -> None:
        """Basic triangle containment checks."""
        contains = algorithms.point_in_polygon_convex
        triangle = ((0, 0), (2, 0), (1, 2))

        self.assertTrue(contains((1, 1), triangle),
                        'Point in triangle')
        self.assertTrue(contains((0, 0), triangle),
                        'Point on vertex #1')
        self.assertTrue(contains((0.5, 1), triangle),
                        'Point on edge #3')
        self.assertFalse(contains((-2, 0), triangle),
                         'Point beyond edge #1')
        self.assertFalse(contains((2, 1), triangle),
                         'Point beyond edge #2')
        self.assertFalse(contains((0, 1), triangle),
                         'Point beyond edge #3')

    def test_triangle_degenerate(self) -> None:
        """Test using degenerate triangles."""
        contains = algorithms.point_in_polygon_convex
        line = ((0, 0), (1, 1))
        degenerate_triangle = ((0, 0), (1, 1), (2, 2))

        with self.assertRaises(ValueError, msg='Input is line'):
            _ = contains((0.5, 0.5), line)

        # NOTE: While it is debatable whether the following should be a valid
        # triangle, it is not up to this function to determine that.
        # Since point_in_polygon_convex also covers edges, this should still
        # succeed for points on the edges, even though it has no area.
        self.assertTrue(contains((0.5, 0.5), degenerate_triangle),
                        'Point on edges #1 and #3')
        self.assertFalse(contains((0, 2), degenerate_triangle),
                         'Point beyond edges #1 and #2')

    def test_triangle_sizes(self) -> None:
        """Test using extreme triangle sizes."""
        contains = algorithms.point_in_polygon_convex
        triangle_tiny = ((0, 0), (1e-6, 1e-6), (0.5e-6, 3e-6))
        triangle_huge = ((-2e12, -1e12), (2e12, 0), (-1e12, 3e12))

        self.assertTrue(contains((0.5e-6, 0.5e-6), triangle_tiny),
                        'Point in triangle')
        self.assertFalse(contains((-1e-6, 0), triangle_tiny),
                         'Point beyond edge #3')
        self.assertTrue(contains((0, 0), triangle_huge),
                        'Point in triangle')
        self.assertFalse(contains((-2e12, 0), triangle_huge),
                         'Point beyond edge #1')

    def test_polygon(self) -> None:
        """Test using a slightly irregular, six-sided polygon."""
        contains = algorithms.point_in_polygon_convex
        hexagon = ((2, 0), (1, 2), (-1, 2), (-2, 0), (-1, -2), (1, -2))

        self.assertTrue(contains((0, 0), hexagon),
                        'Point in polygon')
        self.assertTrue(contains((1, 2), hexagon),
                        'Point on vertex #2')
        self.assertTrue(contains((1.5, 1), hexagon),
                        'Point on edge #1')
        self.assertFalse(contains((1e-6, 2+1e-6), hexagon),
                         'Point beyond edge #2')


class TestPointOnLine(unittest.TestCase):
    """Tests for the `point_on_line` algorithm."""

    def test_basic(self) -> None:
        """Basic tests for point containment."""
        on_line = algorithms.point_on_line

        line = (-1, -1), (2, 2)
        points = [((0, 0), True,), ((1, 0), False),
                  ((1, 1), True), ((3, 3), False)]

        for index, data in enumerate(points):
            point, value = data
            self.assertTrue(on_line(line, point) == value,
                            f'Point #{index+1}: expected {value}, '
                            f'got {not value}')


class TestPointWithinRange(unittest.TestCase):
    """Tests for the `point_within_range` algorithm."""

    def test_basic(self) -> None:
        """Test basic distance checks."""
        in_range = algorithms.point_within_range

        points = [((0, 0), (1, 0), 1, 0.99),
                  ((1, 0), (-1, 5), 5.5, 5.3),
                  ((5, 2), (-2, 5), 7.61578, 7.61577),
                  ((0, 0), (0, 0.01), 0.01, 0),
                  ((120, 152), (-120, 148), 240.034, 240.033),
                  ((1, 0), (1, 1), 1, 0.99)]

        for pt_a, pt_b, dist_true, dist_false in points:
            self.assertTrue(in_range(pt_a, pt_b, dist_true),
                            f'{pt_a} to {pt_b} (True)')
            self.assertFalse(in_range(pt_a, pt_b, dist_false),
                             f'{pt_a} to {pt_b} (False)')


class TestRectangleIntersection(unittest.TestCase):
    """Tests for the `rectangle_intersection` algorithm."""

    def test_basic(self) -> None:
        """Test basic rectangles."""
        overlap = algorithms.rectangle_intersection

        rect_a = 10, 30, 20, 40
        rect_b = 10, 20, 20, 40
        rect_c = 40, 50, 45, 60
        rect_d = 0, 40, 10, 50

        self.assertTrue(overlap(rect_a, rect_b),
                        'Simple overlap')
        self.assertFalse(overlap(rect_a, rect_c),
                         'Disjoint rectangles')
        self.assertTrue(overlap(rect_a, rect_d),
                        'Second contained in first')

    def test_degenerate(self) -> None:
        """Test using zero-length rectangles."""
        overlap = algorithms.rectangle_intersection

        rect_a = 10, 20, 10, 20
        rect_b = 10, 20, 15, 15
        rect_c = 15, 15, 10, 20
        rect_d = 15, 15, 15, 15

        self.assertTrue(overlap(rect_a, rect_b),
                        'Second rectangle has no height')
        # pylint: disable=arguments-out-of-order
        self.assertTrue(overlap(rect_b, rect_a),
                        'First rectangle has no height')
        self.assertTrue(overlap(rect_a, rect_c),
                        'Second rectangle has no width')
        self.assertTrue(overlap(rect_c, rect_a),
                        'First rectangle has no width')
        self.assertTrue(overlap(rect_a, rect_d),
                        'Second rectangle is a single point')
        self.assertTrue(overlap(rect_d, rect_a),
                        'First rectangle is a single point')


class TestRotate2D(unittest.TestCase):
    """Tests for the `rotate_2d` algorithm."""

    def test_basic(self) -> None:
        """Test basic points."""
        rotate = algorithms.rotate_2d

        new_pos = rotate((1.0, 1.0), 180.0, (0.0, 0.0))
        error = algorithms.dist_2d(new_pos, (-1.0, -1.0))
        self.assertAlmostEqual(error, 0.0)
        new_pos = rotate((1.0, 1.0), 180.0, (1.0, 0.0))
        error = algorithms.dist_2d(new_pos, (1.0, -1.0))
        self.assertAlmostEqual(error, 0.0)


class TestSplitLine(unittest.TestCase):
    """Tests for the `split_line` algorithm."""

    def test_basic(self) -> None:
        """Test basic lines."""
        split = algorithms.split_line

        line_a = (0, 0), (0, 10)
        line_b = (0, 0), (10, 0)
        line_c = (0, 0, 0), (1, 1, 1)

        line_a_2 = (0.0, 0.0), (0.0, 5.0), (0.0, 10.0)
        line_b_4 = (0.0, 0.0), (2.5, 0.0), (5.0, 0.0), (7.5, 0.0), (10.0, 0.0)
        line_c_2 = (0.0, 0.0, 0.0), (0.5, 0.5, 0.5), (1.0, 1.0, 1.0)

        self.assertTupleEqual(split(line_a, 2), line_a_2,
                              'Split horizontal (2)')
        self.assertTupleEqual(split(line_b, 4), line_b_4,
                              'Split vertical (4)')
        for test, ref in zip(split(line_c, 2), line_c_2):
            self.assertEqual(len(test), len(ref))
            for index, v_test in enumerate(test):
                self.assertAlmostEqual(v_test, ref[index])
        with self.assertRaises(ValueError):
            _ = split(line_a, 0)

    def test_diagonal(self) -> None:
        """Test using diagonal lines."""
        split = algorithms.split_line

        line = (-5.0, -2.5), (5.0, 2.5)

        x_vars = tuple(float(i) for i in range(-5, 6))
        y_vars = tuple(float(i) / 2 for i in range(-5, 6))
        line_split = tuple(zip(x_vars, y_vars))

        self.assertTupleEqual(split(line, 10), line_split,
                              'Split diagonal')
