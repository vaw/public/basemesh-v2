"""Test cases for the BASEchange module."""

import copy
import os
import unittest
import tempfile
from typing import List, cast, Tuple, Type, TypeVar

# pylint: disable=import-error
import basechange
from basechange.io import BaseChainCrossSection

T = TypeVar('T')


class BMGParseTest(unittest.TestCase):
    """Test the I/O component for BASEchain geometry files."""

    def test_file_basic(self) -> None:
        """Test reading an example BMG file into a data container."""
        cross_sections: List[BaseChainCrossSection] = []
        with basechange.BaseChainReader(
                'tests/data/basechange/h-1_topo.bmg') as parser:
            cross_sections = list(parser.cross_sections())
        self.assertEqual(len(cross_sections), 44)
        first = cross_sections[0]
        self.assertEqual(first.name, 'Qp1')
        self.assertListEqual(first.node_coords, [
            (0.0, 6.0), (0.0, 0.0), (20.0, 0.0), (20.0, 6.0)])

    def test_fields(self) -> None:
        """Ensure all canonical field names for BMG files are parsed.

        This test must be updated to include any new additions to the
        BMG file format.
        """

        def check_field(obj: T, type_: Type[T], value: T) -> None:
            """A helper method to clean up the test case body.

            This checks both the type of a variable and its value.
            """
            self.assertIsInstance(obj, type_)
            self.assertEqual(obj, value)

        cross_sections: List[BaseChainCrossSection] = []
        with basechange.BaseChainReader(
                'tests/data/basechange/all-fields.bmg') as parser:
            cross_sections = list(parser.cross_sections())
        self.assertEqual(len(cross_sections), 1)
        section = cross_sections[0]

        check_field(section.name, str, 'Cs_1')
        check_field(section.distance_coord, float, 1.234)
        check_field(section.sternberg, float, 0.01)
        check_field(section.reference_height, float, 100.0)
        check_field(section.orientation_angle, float, -20.0)
        check_field(section.bed_form_factor, float, 0.9)
        check_field(section.theta_critic, float, -0.5)
        check_field(section.friction_calibration_factor, float, 1.2)
        check_field(section.default_friction, float, 30.0)
        check_field(section.table_fixed_lower_limit, float, -5.0)
        check_field(section.interpolated, bool, False)

        # left_point_global_coords
        self.assertIsInstance(section.left_point_global_coords, tuple)
        lpgc = cast(Tuple[float, float, float],
                    section.left_point_global_coords)
        for coord in lpgc:
            self.assertIsInstance(coord, float)
        self.assertTupleEqual(lpgc, (-50.0, 40.0, 30.0))
        # node_coords
        self.assertIsInstance(section.node_coords, list)
        for node in section.node_coords:
            self.assertIsInstance(node, tuple)
            for coord in node:
                self.assertIsInstance(coord, float)
        self.assertListEqual(section.node_coords, [
            (0.0, 10.0), (1.0, 2.0), (10.0, 1.0), (19.0, 3.0), (20.0, 10.0)])
        # interpolateion_fixpoints
        self.assertIsInstance(section.interpolation_fixpoints, list)
        assert section.interpolation_fixpoints is not None
        for point in section.interpolation_fixpoints:
            self.assertIsInstance(point, int)
        self.assertListEqual(section.interpolation_fixpoints, [3, 4])
        # water_flow_range
        self.assertIsInstance(section.water_flow_range, tuple)
        assert section.water_flow_range is not None
        for coord in section.water_flow_range:
            self.assertIsInstance(coord, float)
        self.assertTupleEqual(section.water_flow_range, (1.0, 19.0))
        # main_channel_range
        self.assertIsInstance(section.main_channel_range, tuple)
        assert section.main_channel_range is not None
        for coord in section.main_channel_range:
            self.assertIsInstance(coord, float)
        self.assertTupleEqual(section.main_channel_range, (1.0, 19.0))
        # bottom_range
        self.assertIsInstance(section.bottom_range, tuple)
        assert section.bottom_range is not None
        for coord in section.bottom_range:
            self.assertIsInstance(coord, float)
        self.assertTupleEqual(section.bottom_range, (1.0, 10.0))
        # active_range
        self.assertIsInstance(section.active_range, tuple)
        assert section.active_range is not None
        for coord in section.active_range:
            self.assertIsInstance(coord, float)
        self.assertTupleEqual(section.active_range, (0.0, 20.0))
        # friction_ranges
        self.assertIsInstance(section.friction_ranges, list)
        assert section.friction_ranges is not None
        for range_ in section.friction_ranges:
            self.assertIsInstance(range_, tuple)
            for coord in range_:
                self.assertIsInstance(coord, float)
        self.assertListEqual(
            section.friction_ranges, [(0.0, 10.0), (10.0, 20.0)])
        # table_fixed_points
        self.assertIsInstance(section.table_fixed_points, list)
        assert section.table_fixed_points is not None
        for value in section.table_fixed_points:
            self.assertIsInstance(value, float)
        self.assertListEqual(section.table_fixed_points, [1.0, 4.0])

    def test_index_fields(self) -> None:
        """Test the "_slice_indexes" field synonyms."""
        cross_sections: List[BaseChainCrossSection] = []
        with basechange.BaseChainReader(
                'tests/data/basechange/index-fields.bmg') as parser:
            cross_sections = list(parser.cross_sections())
        self.assertEqual(len(cross_sections), 1)
        section = cross_sections[0]

        self.assertEqual(section.name, 'Cs_2')
        self.assertEqual(section.distance_coord, 2.345)

        # water_flow_slice_indexes
        self.assertIsInstance(section.water_flow_slice_indexes, tuple)
        assert section.water_flow_slice_indexes is not None
        for index in section.water_flow_slice_indexes:
            self.assertIsInstance(index, int)
        self.assertNotIn(0, section.water_flow_slice_indexes)
        self.assertTupleEqual(section.water_flow_slice_indexes, (2, 4))
        # main_channel_slice_indexes
        self.assertIsInstance(section.main_channel_slice_indexes, tuple)
        assert section.main_channel_slice_indexes is not None
        for index in section.main_channel_slice_indexes:
            self.assertIsInstance(index, int)
        self.assertNotIn(0, section.main_channel_slice_indexes)
        self.assertTupleEqual(section.main_channel_slice_indexes, (2, 4))
        # bottom_slice_indexes
        self.assertIsInstance(section.bottom_slice_indexes, tuple)
        assert section.bottom_slice_indexes is not None
        for index in section.bottom_slice_indexes:
            self.assertIsInstance(index, int)
        self.assertNotIn(0, section.bottom_slice_indexes)
        self.assertTupleEqual(section.bottom_slice_indexes, (2, 3))
        # active_slice_indexes
        self.assertIsInstance(section.active_slice_indexes, tuple)
        assert section.active_slice_indexes is not None
        for index in section.active_slice_indexes:
            self.assertIsInstance(index, int)
        self.assertNotIn(0, section.active_slice_indexes)
        self.assertTupleEqual(section.active_slice_indexes, (1, 5))
        # friction_slice_indexes
        self.assertIsInstance(section.friction_slice_indexes, list)
        assert section.friction_slice_indexes is not None
        for range_ in section.friction_slice_indexes:
            self.assertIsInstance(range_, tuple)
            for index in range_:
                self.assertIsInstance(index, int)
            self.assertNotIn(0, range_)
        self.assertListEqual(section.friction_slice_indexes, [(1, 3), (3, 5)])


class BMGWriteTest(unittest.TestCase):
    """Test the writer class."""

    def _compare_files(self, test_file: str, reference_file: str) -> None:
        """Compare two files line-by-line."""
        with open(test_file, encoding='utf-8') as file_a:
            with open(reference_file, encoding='utf-8') as file_b:
                iter_b = iter(file_b)
                for index, line_a in enumerate(file_a):

                    # Get the matching line in the reference file
                    try:
                        line_b = next(iter_b)
                    except StopIteration:
                        self.fail('Test file has more lines than reference')
                        return

                    # Ignore comments
                    if line_a.startswith('//'):
                        continue

                    # Compare the two files' lines
                    self.assertSequenceEqual(
                        line_a, line_b, f'Mismatch in line #{index+1}')

                # Check for additional lines in the reference file
                msg = 'Test file has fewer lines than reference'
                with self.assertRaises(StopIteration, msg=msg):
                    _ = next(iter_b)

    def test_file_basic(self) -> None:
        """Test writing a simple file."""
        reference = 'tests/data/basechange/write_simple.bmg'
        with tempfile.TemporaryDirectory() as tempdir:
            filepath = os.path.abspath(os.path.join(tempdir, 'test.bmg'))
            with basechange.BaseChainWriter(filepath) as outfile:

                # Define cross section
                geometry = basechange.ChannelGeometry()
                nodes = (0, 4), (1, 2), (2, 4), (3, 0)
                section = basechange.CrossSection(
                    'CS_x', [basechange.Vertex(*pos) for pos in nodes])
                for index in range(20):
                    section = copy.deepcopy(section)
                    section.name = f'CS_{index}'
                    section.flow_axis_coord = index*100
                    geometry.add_cross_section(section)

                outfile.write_channel(geometry)
            self._compare_files(filepath, reference)


class BaseChangeInterface(unittest.TestCase):
    """Test the primary class interfaces of the BASEchange module."""

    def test_position_absolute(self) -> None:
        """Test absolute positioning of cross sections."""
        filepath = 'tests/data/basechange/positioning_absolute.bmg'
        geom = basechange.io.channel_from_basechain(filepath)
        self.assertEqual(len(geom.cross_sections), 11)
        first = geom.cross_sections[0]
        self.assertTrue(first.geo_referenced)

        transformed = [s.position_absolute() for s in geom.cross_sections]

        # Check random transformed points
        section = transformed[0]
        self.assertTupleEqual(section[1], (2.0, 0.0, 2.0))
        section = transformed[2]
        self.assertTupleEqual(section[3], (9.0, 3.0, 4.0))
        section = transformed[5]
        self.assertTupleEqual(
            section[2], (13.828427124746, 16.828427124746, 2.0))
        section = transformed[8]
        self.assertTupleEqual(section[0], (2.0, 14.0, 4.0))
        section = transformed[10]
        self.assertTupleEqual(section[3], (-10.0, 20.0, 4.0))

    def test_position_relative(self) -> None:
        """Test relative positioning of cross sections."""
        filepath = 'tests/data/basechange/positioning_relative.bmg'
        geom = basechange.io.channel_from_basechain(filepath)
        self.assertEqual(len(geom.cross_sections), 11)

        # Transform cross sections
        transformed = [s.position_relative() for s in geom.cross_sections]

        # Check various transformed points
        section = transformed[0]
        self.assertTupleEqual(section[1], (0.0, -1.0, 2.0))
        section = transformed[2]
        self.assertTupleEqual(section[3], (4.0, 3.0, 4.0))
        section = transformed[5]
        self.assertTupleEqual(section[2], (10.0, 1.0, 2.0))
        section = transformed[10]
        self.assertTupleEqual(section[0], (20.0, -3.0, 4.0))
