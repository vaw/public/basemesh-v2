"""Test cases for the Triangle wrapper module."""

import pathlib
import unittest

from test_utils import TempFileTestCase, test_data_path

# pylint: disable=import-error
from basemesh.triangle import errors, io, Node, Segment


def _file(filename: str) -> pathlib.Path:
    return test_data_path('triangle', filename)


class TestEleReader(unittest.TestCase):
    """Tests for the io.read_ele() method."""

    def test_missing(self) -> None:
        """Reading a nonexistant file raises FileNotFoundError."""
        with self.assertRaises(FileNotFoundError):
            _ = dict(io.read_ele(_file('missing.ele')))

    def test_box_1(self) -> None:
        """Can read the box.1.ele from the Triangle docs."""
        data = dict(io.read_ele(_file('website/box.1.ele')))
        self.assertEqual(len(data), 12)
        # Test node indices
        self.assertTupleEqual(data[1].nodes, (7, 11, 8))
        self.assertTupleEqual(data[5].nodes, (3, 7, 10))
        self.assertTupleEqual(data[10].nodes, (5, 10, 7))
        self.assertTupleEqual(data[12].nodes, (8, 4, 12))
        # Test lack of attributes
        for id_ in (1, 3, 6, 12):
            self.assertTrue(len(data[id_].attributes) == 0)


class TestNodeReader(unittest.TestCase):
    """Tests for the io.read_node() method."""

    def test_missing(self) -> None:
        """Reading a nonexistant file raises FileNotFoundError."""
        with self.assertRaises(FileNotFoundError):
            _ = dict(io.read_node(_file('missing.node')))

    def test_box_1(self) -> None:
        """Can read box.1.node from the Triangle docs."""
        data = dict(io.read_node(_file('website/box.1.node')))
        self.assertEqual(len(data), 12)
        # Test coordinates
        self.assertTupleEqual(data[1].as_tuple(), (0, 0))
        self.assertTupleEqual(data[4].as_tuple(), (3, 3))
        self.assertTupleEqual(data[7].as_tuple(), (2, 1))
        self.assertTupleEqual(data[11].as_tuple(), (3, 1.5))
        # Test boundary markers
        self.assertEqual(data[1].marker, 5)
        self.assertEqual(data[4].marker, 33)
        self.assertEqual(data[12].marker, 1)

    def test_dots_scatter(self) -> None:
        """Can read dots_scatter.node from the Triangle docs."""
        data = dict(io.read_node(_file('website/dots_scatter.node')))
        self.assertEqual(len(data), 100)
        # Test coordinates
        self.assertTupleEqual(data[1].as_tuple(), (0.0476694, 0.809168))
        self.assertTupleEqual(data[25].as_tuple(), (0.372467,  0.586735))
        self.assertTupleEqual(data[99].as_tuple(), (0.0532971, -0.800056))
        # Test lack of boundary markers
        for id_ in (1, 26, 42, 100):
            self.assertIsNone(data[id_].marker)

    def test_dots_scatter_1(self) -> None:
        """Can read dots_scatter.1.v.node from the Triangle docs."""
        data = dict(io.read_node(_file('website/dots_scatter.1.v.node')))
        self.assertEqual(len(data), 187)
        # Test coordinates
        self.assertTupleEqual(data[1].as_tuple(),
                              (-0.64677978809216052, -0.15912669633082885))
        self.assertTupleEqual(data[111].as_tuple(),
                              (0.06868494173420675, -0.10451655347236619))
        self.assertTupleEqual(data[187].as_tuple(),
                              (4.1025027793325153,  -33.699956974647101))
        # Test lack of boundary markers
        for id_ in (1, 7, 12, 166):
            self.assertEqual(data[id_].marker, None)

    def test_spiral(self) -> None:
        """Can read spiral.node from the Triangle docs."""
        data = dict(io.read_node(_file('website/spiral.node')))
        self.assertEqual(len(data), 15)
        # Test coordinates
        self.assertTupleEqual(data[1].as_tuple(), (0, 0))
        self.assertTupleEqual(data[4].as_tuple(), (-1.64, -0.549))
        self.assertTupleEqual(data[15].as_tuple(), (1.36, 3.49))
        # Test lack of boundary markers
        for id_ in (1, 2, 15):
            self.assertIsNone(data[id_].marker)

    def test_face_1(self) -> None:
        """Invalid file face.1.node raises appropriate exception."""
        # NOTE: This file is invalid despite being part of the docs; the header
        # does not match the Triangle specification for the .NODE format.
        with self.assertRaises(errors.ParserError):
            _ = dict(io.read_node(_file('website/face.1.node')))

    def test_dots(self) -> None:
        """Invalid file dots.node raises appropriate exception."""
        # NOTE: This file is in POLY format despite being labelled NODE.
        with self.assertRaises(errors.ParserError):
            _ = dict(io.read_node(_file('website/dots.node')))


class TestNodeWriter(TempFileTestCase):
    """Tests for the io.write_node() method."""

    def test_rewrite_box_1(self) -> None:
        """Written file is the same as the original."""
        nodes = dict(io.read_node(_file('website/box.1.node')))
        # Re-write the same file into the temp dir
        path = self.file('test.node')
        io.write_node(path, nodes.values())
        # Read the written file and compare it with the original
        rewritten = dict(io.read_node(path))
        self.assertEqual(len(nodes), len(rewritten))
        for node1, node2 in zip(nodes.values(), rewritten.values()):
            self.assertEqual(node1.id, node2.id)
            self.assertTupleEqual(node1.as_tuple(), node2.as_tuple())
            self.assertTupleEqual(node1.attributes, node2.attributes)
            # NOTE: Markers are not compared as writing them is not supported


class TestPolyWriter(TempFileTestCase):
    """Tests for the io.write_poly() method."""

    def test_square(self) -> None:
        """Manually created box."""
        nodes = [Node(1, 0.0, 0.0, 1.0, marker=10),
                 Node(2, 0.0, 2.0, 2.0, marker=11),
                 Node(3, 2.0, 0.0, 3.0, marker=12),
                 Node(4, 2.0, 2.0, -1.0, marker=13)]
        segments = [Segment(1, 1, 2, marker=20),
                    Segment(2, 1, 3, marker=21),
                    Segment(3, 2, 4, marker=22),
                    Segment(4, 3, 4, marker=23)]
        path = self.file('test.poly')
        io.write_poly(path, nodes, segments, write_markers=True)
