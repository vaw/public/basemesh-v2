"""Tests for the example scripts provided for the meshtool module."""

import os
import pathlib
import subprocess
import sys
from typing import Dict

from test_utils import TempFileTestCase

_REPO = pathlib.Path(__file__).parents[1]
_MESHTOOL_EXAMPLES = _REPO / 'examples' / 'meshtool'


class MeshToolExamples(TempFileTestCase):
    """Try running the example scripts provided for meshtool."""

    def test_example_files(self) -> None:
        """Loop over all example files and generate their meshes."""
        # Create a modified environment in which the meshtool import is on
        # PYTHONPATH by default
        my_env = os.environ.copy()
        path = my_env.get('PYTHONPATH', '')
        if path:
            path += os.pathsep
        path += str(_REPO)
        my_env['PYTHONPATH'] = path

        for example_file in _MESHTOOL_EXAMPLES.iterdir():
            if example_file.suffix == '.py':
                with self.subTest(example_file.stem):
                    self._run_example(example_file, my_env)

    def _run_example(self, path: pathlib.Path, my_env: Dict[str, str]) -> None:
        """Run a single example script."""
        try:
            subprocess.call([sys.executable, str(path)],
                            stdout=subprocess.DEVNULL,
                            cwd=self.temp_dir.name, env=my_env)
        except subprocess.CalledProcessError as err:
            self.fail(f'Script run failed for {path.stem}: {err}')
