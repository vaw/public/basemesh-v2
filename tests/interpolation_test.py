"""Test cases related to mesh interpolation."""

import tempfile
import unittest
from typing import List

from basemesh import Mesh, calculate_element_elevation, interpolate_mesh

# pylint: disable=import-error
from test_utils import test_data_path

_PATH_QM = test_data_path('2dm', 'basic_flat.2dm')
_PATH_EM = test_data_path('2dm', 'basic_elevation.2dm')


class InterpolateNodes(unittest.TestCase):
    """Test node interpolation functionality (BASEMENT 2)."""

    @staticmethod
    def _serialise_mesh(mesh: Mesh) -> List[str]:
        with tempfile.TemporaryDirectory() as tempdir:
            path = tempdir + '/mesh.2dm'
            mesh.save(path)
            with open(path, 'r') as f:
                return f.readlines()

    def test_interpolate_self(self) -> None:
        """Interpolating a mesh with itself does nothing."""
        mesh = Mesh.open(_PATH_QM)
        orig_str = self._serialise_mesh(mesh)
        interpolate_mesh(mesh, mesh)
        interpol_str = self._serialise_mesh(mesh)
        self.assertSequenceEqual(orig_str, interpol_str)

    def test_interpolate_other(self) -> None:
        """Interpolating a mesh updates its node elevations."""
        mesh = Mesh.open(_PATH_QM)
        nodes_2d = sorted(mesh.nodes, key=lambda n: n.id)
        other = Mesh.open(_PATH_EM)
        interpolate_mesh(mesh, other)
        nodes_3d = sorted(mesh.nodes, key=lambda n: n.id)
        # Check X and Y coordinates are the same
        for n1, n2 in zip(nodes_2d, nodes_3d):
            self.assertTupleEqual(n1.pos[:2], n2.pos[:2])
        # Check Z coordinate is interpolated
        self.assertEqual(nodes_3d[0].pos[2], 2.0)
        self.assertEqual(nodes_3d[1].pos[2], 2.5)
        self.assertEqual(nodes_3d[2].pos[2], 3.0)
        self.assertEqual(nodes_3d[3].pos[2], 3.5)
        self.assertEqual(nodes_3d[4].pos[2], 4.0)
        self.assertEqual(nodes_3d[5].pos[2], 2.5)
        self.assertEqual(nodes_3d[6].pos[2], 1.0)
        self.assertEqual(nodes_3d[7].pos[2], 1.5)
        self.assertEqual(nodes_3d[8].pos[2], 2.0)


class TestInterpolateElements(unittest.TestCase):
    """Test element interpolation functionality (BASEMENT 3)."""

    def test_interpolate(self) -> None:
        """Interpolate element elevations based on node elevations."""
        mesh = Mesh.open(_PATH_QM)
        elev = Mesh.open(_PATH_EM)
        elevations = calculate_element_elevation(mesh, elev)
        # Check element elevations are interpolated
        self.assertAlmostEqual(elevations[1], 11/6)
        self.assertAlmostEqual(elevations[2], 13/6)
        self.assertAlmostEqual(elevations[3], 15/6)
        self.assertAlmostEqual(elevations[4], 17/6)
        self.assertAlmostEqual(elevations[5], 19/6)
        self.assertAlmostEqual(elevations[6], 17/6)
        self.assertAlmostEqual(elevations[7], 11/6)
        self.assertAlmostEqual(elevations[8], 9/6)
