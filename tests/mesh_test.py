"""Test cases for mesh I/O."""

import os
import pathlib
import shutil
import unittest
from typing import List

# pylint: disable=import-error
from basemesh import Mesh, MeshElement, mesh_from_triangle, triangle
from basemesh.errors import BasemeshWarning

from test_utils import TempFileTestCase, test_data_path


class MeshFromTriangle(TempFileTestCase):
    """Ensure meshes can be created from Triangle output."""

    def test_from_triangle(self) -> None:
        """Test a mesh can be created from Triangle output."""
        input_ = test_data_path('triangle', 'website', 'box.1')
        mesh = mesh_from_triangle(input_)

        self.assertEqual(len(mesh.nodes), 12)
        self.assertEqual(len(mesh.elements), 12)
        for node_id in (1, 4, 10):
            node = mesh.get_node_by_id(node_id)
            self.assertEqual(node.id, node_id)
            if node.id == 1:
                self.assertTupleEqual(node.pos, (0.0, 0.0, 0.0))
            elif node.id == 4:
                self.assertTupleEqual(node.pos, (3.0, 3.0, 0.0))
            elif node.id == 10:
                self.assertTupleEqual(node.pos, (1.5, 0.0, 0.0))

    def test_triangle_with_elevation(self) -> None:
        """Test 2.5D interpolation parsing via node attributes."""
        # Copy the input file to a temporary location for processing
        src = test_data_path('triangle', 'box-with-elevation.poly')
        shutil.copy(str(src), self.temp_dir.name)
        input_ = self.file(src.name)

        # Run Triangle and convert to a mesh
        triangle.run_triangle(input_, '-pc', redirect_stdout=None)
        mesh = mesh_from_triangle(input_.with_suffix('.1'), elevation=1)

        self.assertEqual(len(mesh.nodes), 9)
        self.assertEqual(len(mesh.elements), 12)
        node = mesh.get_node_by_id(1)
        self.assertEqual(node.id, 1)
        self.assertTupleEqual(node.pos, (-2, -2, 0.5))
        node = mesh.get_node_by_id(7)
        self.assertEqual(node.id, 7)
        self.assertTupleEqual(node.pos, (1, -1, 0.75))


class MeshReader(unittest.TestCase):
    """Test cases for loading 2DM meshes into BASEmesh's data type."""

    def test_open_mesh(self) -> None:
        """Can open and read a valid 2DM mesh."""
        mesh = Mesh.open(test_data_path('2dm', 'basic_flat.2dm'))

        self.assertTrue(len(mesh.nodes) == 9)
        self.assertTrue(len(mesh.elements) == 8)

        node = mesh.get_node_by_id(1)
        self.assertEqual(node.id, 1)
        self.assertTupleEqual(node.pos, (0.0, 0.0, 0.0))
        node = mesh.get_node_by_id(5)
        self.assertEqual(node.id, 5)
        self.assertTupleEqual(node.pos, (-1.0, -1.0, 0.0))

        ele = mesh.get_element_by_id(1)
        self.assertEqual(ele.id, 1)
        self.assertEqual(ele.materials, ())
        self.assertListEqual([n.id for n in ele.nodes], [1, 9, 8])
        ele = mesh.get_element_by_id(4)
        self.assertEqual(ele.id, 4)
        self.assertEqual(ele.materials, ())
        self.assertListEqual([n.id for n in ele.nodes], [1, 4, 3])

    def test_no_num_materials_per_elem(self) -> None:
        """Can open a mesh with no NUM_MATERIALS_PER_ELEM flag."""
        # Reading these files should work but come with a warning since the
        # output will no longer match the input
        with self.assertWarns(UserWarning):
            mesh = Mesh.open(test_data_path('2dm', 'no_nmpe_flag.2dm'))

        self.assertTrue(len(mesh.nodes) == 4)
        self.assertTrue(len(mesh.elements) == 2)

        node = mesh.get_node_by_id(1)
        self.assertEqual(node.id, 1)
        self.assertTupleEqual(node.pos, (0.0, 0.0, 1.0))
        node = mesh.get_node_by_id(2)
        self.assertEqual(node.id, 2)
        self.assertTupleEqual(node.pos, (1.0, 0.0, 0.5))
        node = mesh.get_node_by_id(3)
        self.assertEqual(node.id, 3)
        self.assertTupleEqual(node.pos, (1.0, 1.0, 1.0))
        node = mesh.get_node_by_id(4)
        self.assertEqual(node.id, 4)
        self.assertTupleEqual(node.pos, (0.0, 1.0, 0.5))

        ele = mesh.get_element_by_id(1)
        self.assertEqual(ele.id, 1)
        self.assertEqual(ele.materials, ())
        self.assertListEqual([n.id for n in ele.nodes], [1, 2, 3])
        ele = mesh.get_element_by_id(2)
        self.assertEqual(ele.id, 2)
        self.assertEqual(ele.materials, ())
        self.assertListEqual([n.id for n in ele.nodes], [4, 1, 3])


class MeshWriter(TempFileTestCase):
    """Test cases for writing 2DM meshes with BASEmesh."""

    _OUTPUT = 'output.2dm'

    def test_mesh_is_one_indexed(self) -> None:
        """A zero-based mesh should be converted to one-based on save."""
        input_ = test_data_path('2dm', 'zero_indexed.2dm')
        output = self.file(self._OUTPUT)
        with self.assertWarns(BasemeshWarning):
            Mesh.open(input_).save(output)

        mesh = Mesh.open(output)
        self.assertTrue(len(mesh.nodes))
        node = mesh.get_node_by_id(1)
        self.assertEqual(node.id, 1)
        self.assertTupleEqual(node.pos, (0.0, 0.0, 0.0))
        with self.assertRaises(KeyError):
            mesh.get_node_by_id(0)

        self.assertTrue(len(mesh.elements) == 8)
        ele = mesh.get_element_by_id(1)
        self.assertEqual(ele.id, 1)
        self.assertListEqual([n.id for n in ele.nodes], [1, 9, 8])
        with self.assertRaises(KeyError):
            mesh.get_element_by_id(0)

        self.assertTrue(len(mesh.node_strings) == 1)
        ns = mesh.node_strings['test']
        self.assertListEqual([n.id for n in ns], [1, 3, 5, 7])

    def test_mesh_has_no_trailing_newline(self) -> None:
        """BASEMENT 3 does not allow trailing newlines for its 2DMs."""
        input_ = test_data_path('2dm', 'basic_elevation.2dm')
        output = self.file(self._OUTPUT)
        Mesh.open(input_).save(output)

        self.assertFalse(_file_has_trailing_newline(output))

    def test_no_multiline_node_strings(self) -> None:
        """Unlike the 2DM standard we require all nodes on one line."""
        input_ = test_data_path('2dm', 'multiline_node_string.2dm')
        output = self.file(self._OUTPUT)
        Mesh.open(input_).save(output)

        ns_tokens: List[str] = []
        with open(output, 'r', encoding='utf-8') as file_:
            for line in file_:
                if not line.startswith('NS'):
                    continue
                ns_tokens = line.split()

        self.assertEqual(len(ns_tokens), 14)
        self.assertEqual(ns_tokens[0], 'NS')
        # Check that all nodes are present and on a single line
        self.assertSequenceEqual(list(map(int, ns_tokens[1:-1])),
                                 [1, 3, 2, 4, 5, 7, 6, 8, 9, 11, 10, -12])
        self.assertEqual(ns_tokens[-1], 'example_node_string')


class MeshModification(unittest.TestCase):
    """Tests for in-place modification of meshes."""

    def test_move_nodes(self) -> None:
        """Nodes can be moved in place."""
        mesh = Mesh.open(test_data_path('2dm', 'basic_elevation.2dm'))
        node = mesh.get_node_by_id(1)
        node.pos = (1.0, 1.0, 1.0)
        self.assertTupleEqual(node.pos, (1.0, 1.0, 1.0))

    def test_unflip_elements(self) -> None:
        """Elements are unflipped on creation."""
        mesh = Mesh.open(test_data_path('2dm', 'basic_elevation.2dm'))
        ele = mesh.get_element_by_id(1)
        mesh.remove_element(ele)
        n1, n2, n3 = ele.nodes
        with self.assertWarns(BasemeshWarning):
            new_ele = MeshElement(mesh, (n2, n1, n3), 1, *ele.materials)
        self.assertListEqual([n.id for n in new_ele.nodes], [2, 1, 3])

    def test_add_element_via_tuple(self) -> None:
        """Test the point-based add_element overload."""
        mesh = Mesh()
        n1 = mesh.add_node((0.0, 0.0, 0.0))
        ele = mesh.add_element(((0, 0, 0), (1, 0, 0), (1, 1, 0)), 1)
        self.assertEqual(ele.id, 1)
        self.assertListEqual([n.id for n in ele.nodes], [1, 2, 3])
        self.assertEqual(ele.nodes[0], n1)

class MeshEntityProperties(unittest.TestCase):
    """Node and element properties."""

    def test_element_area(self) -> None:
        """Element area is calculated correctly."""
        mesh = Mesh.open(test_data_path('2dm', 'basic_elevation.2dm'))
        ele = mesh.get_element_by_id(1)
        self.assertAlmostEqual(ele.area, 2.0)


def _file_has_trailing_newline(path: pathlib.Path) -> bool:
    """Return True if the file has a trailing newline."""
    with open(path, 'rb') as file_:
        file_.seek(0, os.SEEK_END)
        file_.seek(-1, os.SEEK_CUR)
        # NOTE: This works for both Windows and Unix newlines as the "\n"
        # character is the last character in the file for both.
        return file_.read(1) == b'\n'
