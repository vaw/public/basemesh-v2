"""Test cases for the PSLGBuilder utility class."""

import unittest

# pylint: disable=import-error
from basemesh import PSLGBuilder, Segment, Vertex
from basemesh.errors import BasemeshWarning


class TestBuildProcess(unittest.TestCase):
    """Tests for adding geometry and building Triangle input."""

    def test_empty(self) -> None:
        """An empty PSLGBuilder has no geometry."""
        builder = PSLGBuilder()
        self.assertEqual(builder.vertex_count, 0)
        self.assertEqual(builder.segment_count, 0)

    def test_add_vertex(self) -> None:
        """Can add a vertex to the PSLGBuilder."""
        builder = PSLGBuilder()
        builder.add_vertex(Vertex((1, 2)))
        self.assertEqual(builder.vertex_count, 1)
        self.assertEqual(builder.segment_count, 0)

    def test_add_vertices(self) -> None:
        """Can add multiple vertices to the PSLGBuilder."""
        builder = PSLGBuilder()
        builder.add_vertices([Vertex((1, 2)), Vertex((3, 4))])
        self.assertEqual(builder.vertex_count, 2)
        self.assertEqual(builder.segment_count, 0)

    def test_add_segment(self) -> None:
        """Can add a segment to the PSLGBuilder."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, i)) for i in range(2)]
        builder.add_vertices(vertices)
        builder.add_segment(Segment(*vertices))
        self.assertEqual(builder.vertex_count, 2)
        self.assertEqual(builder.segment_count, 1)

    def test_add_segments(self) -> None:
        """Can add multiple segments to the PSLGBuilder."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, i)) for i in range(4)]
        builder.add_vertices(vertices)
        builder.add_segments([Segment(*vertices[:2]), Segment(*vertices[2:])])
        self.assertEqual(builder.vertex_count, 4)
        self.assertEqual(builder.segment_count, 2)

    def test_clear(self) -> None:
        """Can clear the PSLGBuilder."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, i)) for i in range(4)]
        builder.add_vertices(vertices)
        builder.add_segments([Segment(*vertices[:2]), Segment(*vertices[2:])])
        builder.clear()
        self.assertEqual(builder.vertex_count, 0)
        self.assertEqual(builder.segment_count, 0)

    def test_build(self) -> None:
        """Building produces PSLG output."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, i)) for i in range(4)]
        builder.add_vertices(vertices)
        builder.add_segments([Segment(*vertices[:2]), Segment(*vertices[2:])])
        nodes, segments = builder.build()
        self.assertEqual(len(nodes), 4)
        self.assertEqual(len(segments), 2)


class TestBuilderCleaning(unittest.TestCase):
    """Tests for the builder class's geometry cleaning system."""

    def test_merge_vertices(self) -> None:
        """Test the vertex deduplication step."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, i)) for i in range(4)]
        builder.add_vertices(vertices)
        builder.add_vertex(Vertex((1, 1)))
        builder.add_vertex(Vertex((1.5, 1)))
        builder.add_vertex(Vertex((1.501, 1)))

        self.assertEqual(builder.vertex_count, 7)
        builder.merge_vertices(tol=0.5)
        self.assertEqual(builder.vertex_count, 5)
        builder.merge_vertices(tol=0.5)
        self.assertEqual(builder.vertex_count, 5)

    def test_merge_coincident_segments(self) -> None:
        """Test for segment deduplication step."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, i)) for i in range(4)]
        builder.add_vertices(vertices)
        builder.add_segment((Segment(*vertices[:2])))
        builder.add_segment((Segment(*vertices[2:])))
        builder.add_segment((Segment(*reversed(vertices[:2]))))
        builder.add_segment((Segment(*reversed(vertices[2:]))))

        self.assertEqual(builder.segment_count, 4)
        builder.merge_coincident_segments()
        self.assertEqual(builder.segment_count, 2)
        builder.merge_coincident_segments()
        self.assertEqual(builder.segment_count, 2)

    def test_insert_intersection_vertices(self) -> None:
        """Test intersection vertex insertion."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, 0)), Vertex((0, -1)),
                    Vertex((-1, 0)), Vertex((0, 1))]
        builder.add_vertices(vertices)
        builder.add_segment(Segment(vertices[0], vertices[2]))
        builder.add_segment(Segment(vertices[1], vertices[3]))

        self.assertEqual(builder.vertex_count, 4)
        self.assertEqual(builder.segment_count, 2)
        builder.insert_intersection_vertices(0.1)
        self.assertEqual(builder.vertex_count, 5)
        self.assertEqual(builder.segment_count, 2)

        nodes, _ = builder.build()
        self.assertTrue(any(n.as_tuple() == (0.0, 0.0) for n in nodes))

    def test_inserted_vertex_endpoint_distance(self) -> None:
        """Test that no vertices are inserted close to endpoints."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, 0)), Vertex((0.5, -1)),
                    Vertex((-1, 0)), Vertex((0.5, 1))]
        builder.add_vertices(vertices)
        builder.add_segment(Segment(vertices[0], vertices[2]))
        builder.add_segment(Segment(vertices[1], vertices[3]))

        self.assertEqual(builder.vertex_count, 4)
        self.assertEqual(builder.segment_count, 2)
        builder.insert_intersection_vertices(0.5)
        self.assertEqual(builder.vertex_count, 4)
        self.assertEqual(builder.segment_count, 2)
        builder.insert_intersection_vertices(0.4)
        self.assertEqual(builder.vertex_count, 5)
        self.assertEqual(builder.segment_count, 2)

    def test_inserted_elevation_warning(self) -> None:
        """Test a warning is emitted for different line heights."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, 0, 1)), Vertex((0, -1, 2)),
                    Vertex((-1, 0, 1)), Vertex((0, 1, 2))]
        builder.add_vertices(vertices)
        builder.add_segment(Segment(vertices[0], vertices[2]))
        builder.add_segment(Segment(vertices[1], vertices[3]))

        self.assertEqual(builder.vertex_count, 4)
        self.assertEqual(builder.segment_count, 2)
        with self.assertWarns(BasemeshWarning):
            builder.insert_intersection_vertices(0.1)
        self.assertEqual(builder.vertex_count, 5)
        self.assertEqual(builder.segment_count, 2)

    def test_split_segments(self) -> None:
        """Test that nodes are split at any intersection points."""
        builder = PSLGBuilder()
        vertices = [Vertex((1, 0)), Vertex((0, -1)),
                    Vertex((-1, 0)), Vertex((0, 1)),
                    Vertex((0, 0))]
        builder.add_vertices(vertices)
        builder.add_segment(Segment(vertices[0], vertices[2]))
        builder.add_segment(Segment(vertices[1], vertices[3]))

        self.assertEqual(builder.vertex_count, 5)
        self.assertEqual(builder.segment_count, 2)
        self.assertEqual(builder.split_segments(0.1), 2)
        self.assertEqual(builder.vertex_count, 5)
        self.assertEqual(builder.segment_count, 4)

    def test_split_segments_multiple(self) -> None:
        """Segment splitting test with multiple splits per segoment."""

        # Sketch of the input geometry tested here:
        #
        # S1 through S3 are three continuous segments passing through the
        # vertices V2 and V3.
        #
        #                (V5)        (V6)
        #                 |           |
        #                 S2          S3
        #                 |           |
        #    (V1) --S1-- (V2) --S1-- (V3) --S1-- (V4)
        #                 |           |
        #                 S2          S3
        #                 |           |
        #                (V7)        (V8)
        #
        # As part of the test, segments S1 through S3 should disappear and be
        # replaced with seven new segments connecting the nodes directly.

        builder = PSLGBuilder()
        vertices = [Vertex((x, 0)) for x in range(4)]
        vertices.extend(Vertex((x, 1)) for x in range(1, 3))
        vertices.extend(Vertex((x, -1)) for x in range(1, 3))
        builder.add_vertices(vertices)
        segments = [Segment(vertices[0], vertices[3]),
                    Segment(vertices[4], vertices[6]),
                    Segment(vertices[5], vertices[7])]
        builder.add_segments(segments)

        self.assertEqual(builder.vertex_count, 8)
        self.assertEqual(builder.segment_count, 3)
        self.assertEqual(builder.split_segments(0.1), 4)
        self.assertEqual(builder.vertex_count, 8)
        self.assertEqual(builder.segment_count, 7)

    def test_remove_singular_segments(self) -> None:
        """Test singular segment removal"""
        builder = PSLGBuilder()
        vertices = [Vertex((1, 0)), Vertex((0, 0)), Vertex((1, 0))]
        builder.add_vertices(vertices)
        builder.add_segment(Segment(vertices[0], vertices[0]))
        builder.add_segment(Segment(vertices[0], vertices[1]))
        builder.add_segment(Segment(vertices[0], vertices[2]))

        self.assertEqual(builder.vertex_count, 3)
        self.assertEqual(builder.segment_count, 3)
        builder.remove_singular_segments()
        self.assertEqual(builder.vertex_count, 3)
        self.assertEqual(builder.segment_count, 1)
