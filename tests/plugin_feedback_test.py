"""Test cases for the custom feedback system in the QGIS plugin.

This feedback system wraps the `QgsFeedback` type and extends it for
multi-step progress reporting in sub-tasks. These features are tested
here.
"""

import typing
import unittest

# pylint: disable=import-error

if typing.TYPE_CHECKING:
    from qgis_plugin._feedback import Feedback, MultiStepFeedback
else:
    import pathlib
    import sys
    # NOTE: Hacky workaround to only import the feedback module and not any of
    # the neighbouring ones requiring qgis.core.
    path_to_submodule = pathlib.Path(__file__).parents[1] / 'qgis_plugin'
    sys.path.insert(0, str(path_to_submodule))
    from _feedback import Feedback, MultiStepFeedback  # type: ignore


class FeedbackTests(unittest.TestCase):
    """Tests for the basic `Feedback` class."""

    def test_cancel(self) -> None:
        feedback = Feedback()
        self.assertFalse(feedback.isCanceled())
        feedback.cancel()
        self.assertTrue(feedback.isCanceled())

    def test_progress(self) -> None:
        feedback = Feedback()
        self.assertEqual(feedback.progress(), 0.0)
        feedback.setProgress(50.0)
        self.assertEqual(feedback.progress(), 50.0)
        feedback.setProgress(200.0)
        self.assertEqual(feedback.progress(), 100.0)
        feedback.setProgress(-100.0)
        self.assertEqual(feedback.progress(), 0.0)

    def test_busy_prop(self) -> None:
        feedback = Feedback()
        self.assertFalse(feedback.isBusy())
        feedback.setBusy(True)
        self.assertTrue(feedback.isBusy())
        feedback.setBusy(False)
        self.assertFalse(feedback.isBusy())

    def test_label_prop(self) -> None:
        feedback = Feedback()
        self.assertEqual(feedback.label(), '')
        feedback.setLabel('foo')
        self.assertEqual(feedback.label(), 'foo')
        feedback.setLabel('bar')
        self.assertEqual(feedback.label(), 'bar')

    def test_busy_context_manager(self) -> None:
        feedback = Feedback()
        self.assertFalse(feedback.isBusy())
        with feedback.busy():
            self.assertTrue(feedback.isBusy())
        self.assertFalse(feedback.isBusy())
        with feedback.busy('foo'):
            self.assertTrue(feedback.isBusy())
            self.assertEqual(feedback.label(), 'foo')
        self.assertFalse(feedback.isBusy())
        self.assertEqual(feedback.label(), '')


class MultiStepFeedbackTest(unittest.TestCase):
    """Tests for the extra tools of the MultiStepFeedback subclass."""

    def test_label(self) -> None:
        feedback = MultiStepFeedback()
        self.assertEqual(feedback.label(), 'Step 1 of 1')
        feedback.setLabel('foo')
        self.assertEqual(feedback.label(), 'Step 1 of 1: foo')
        feedback.setStepCount(2)
        self.assertEqual(feedback.label(), 'Step 1 of 2: foo')
        feedback.setLabel('bar')
        self.assertEqual(feedback.label(), 'Step 1 of 2: bar')

    def test_step_count(self) -> None:
        feedback = MultiStepFeedback()
        self.assertEqual(feedback.stepCount(), 1)
        feedback.setStepCount(2)
        self.assertEqual(feedback.stepCount(), 2)
        feedback.setStepCount(1)
        self.assertEqual(feedback.stepCount(), 1)

    def test_step(self) -> None:
        feedback = MultiStepFeedback()
        self.assertEqual(feedback.step(), 1)
        feedback.setStep(2)
        self.assertEqual(feedback.step(), 1)
        feedback.setStepCount(2)
        feedback.setStep(2)
        self.assertEqual(feedback.step(), 2)
        feedback.setStep(1)
        self.assertEqual(feedback.step(), 1)

    def test_2_step_scaling(self) -> None:
        feedback = MultiStepFeedback()
        feedback.setStepCount(2)
        self.assertEqual(feedback.step(), 1)
        self.assertEqual(feedback.progress(), 0.0)
        feedback.setProgress(50.0)
        self.assertEqual(feedback.progress(), 25.0)
        feedback.setStep(2)
        self.assertEqual(feedback.step(), 2)
        self.assertEqual(feedback.progress(), 50.0)
        feedback.setProgress(50.0)
        self.assertEqual(feedback.progress(), 75.0)
        feedback.setProgress(100.0)
        self.assertEqual(feedback.progress(), 100.0)

    def test_3_step_scaling(self) -> None:
        feedback = MultiStepFeedback()
        feedback.setStepCount(3)
        self.assertEqual(feedback.step(), 1)
        self.assertEqual(feedback.progress(), 0.0)
        feedback.setProgress(50.0)
        self.assertAlmostEqual(feedback.progress(), 16.666666666666668)
        feedback.setStep(2)
        self.assertEqual(feedback.step(), 2)
        self.assertAlmostEqual(feedback.progress(), 33.333333333333336)
        feedback.setProgress(50.0)
        self.assertAlmostEqual(feedback.progress(), 50.0)
        feedback.setStep(3)
        self.assertEqual(feedback.step(), 3)
        self.assertAlmostEqual(feedback.progress(), 66.66666666666667)
        feedback.setProgress(50.0)
        self.assertAlmostEqual(feedback.progress(), 83.33333333333334)
        feedback.setProgress(100.0)
        self.assertAlmostEqual(feedback.progress(), 100.0)
