# type: ignore
import math

from meshtool.factories import RectangularMeshFactory


class ConicalDune(RectangularMeshFactory):
    def __init__(self, max_elev=20, min_elev=0.1):
        if min_elev >= max_elev:
            raise ValueError('Maximum elevation must be greater than minimum.')

        # Instantiate RectangularMeshFactory
        super().__init__(1000, 1000, (500, 500))
        self.max_elev = max_elev
        self.min_elev = min_elev

    def elevation_at(self, point):
        x, y = point

        # Expression to describe the bottom elevation, eta = f(x,y)
        eta = self.min_elev
        if 300 < x < 500 and 400 < y < 600:
            eta += (math.sin(math.pi * (x-300)/200)**2
                    * math.sin(math.pi * (y-400)/200)**2) * self.max_elev
        return eta


factory = ConicalDune()
# This examples does not provide any string definitions, the second tuple item
# is therefore discarded.
mesh, _ = factory.build(max_area=50, min_angle=31)
# Save the mesh
mesh.save('conical_dune.2dm')
