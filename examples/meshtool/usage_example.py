# type: ignore
# pylint: disable=invalid-name

import meshtool
from meshtool import Node, Segment


class ExampleFactory(meshtool.AbstractFactory):
    def __init__(self):
        super().__init__()  # Instantiate AbstractFactory

        # Add nodes
        top_left = Node((-100, 100))
        top_right = Node((100, 100))
        bottom_left = Node((-100, -100))
        bottom_right = Node((100, -100))
        midpoint = Node((0, 0))
        self.nodes = [top_left, top_right, midpoint,
                      bottom_left, bottom_right]

        # Add segments
        top = Segment(top_left, top_right)
        left = Segment(top_left, bottom_left)
        right = Segment(top_right, bottom_right)
        bottom = Segment(bottom_left, bottom_right)
        self.segments = [top, left, right, bottom]

    def elevation_at(self, point):
        # f(x, y) = (x² + y²/2) / 100

        x, y = point
        return (x**2 + y**2 / 2) / 100


my_factory = ExampleFactory()
mesh, _ = my_factory.build(max_area=5, min_angle=30)
elevations = meshtool.calculate_element_elevation(mesh, my_factory)
for element in mesh.elements:
    element.materials = *element.materials, elevations[element.id]
mesh.save('example.2dm')
