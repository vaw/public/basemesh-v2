# type: ignore
# pylint: disable=invalid-name

import math

from meshtool.factories import RectangularMeshFactory

# Instantiate the factory. The helper used also defines the mesh domain
factory = RectangularMeshFactory(width=100.0, height=60.0)

# Define the elevation function to use


@factory.set_elevation_func
def ripples(point):
    x, y = point
    # f(x, y) = sin(x) + cos(y) + x² + y²
    return math.sin(x) + math.cos(y) + (x**2 + y**2) / 200 + 10


# Generate the mesh
mesh, _ = factory.build(max_area=0.15)

# Export the mesh
mesh.save('ripple_conic.2dm')
