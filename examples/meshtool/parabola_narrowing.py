# type: ignore

from typing import Any, Dict, List, Tuple

import meshtool
from meshtool.factories import segments_from_perimeter
from basemesh import Mesh


class ParabolaNarrowing(meshtool.AbstractFactory):

    # pylint: disable=invalid-name

    def __init__(self, base_width=60.0, length=250.0,
                 elev_min=0.0, elev_max=20.0,
                 taper_ratio=0.6, taper_start=100.0, taper_end=150.0):
        super().__init__()
        # Ensure the elevation range cannot be negative
        if elev_min >= elev_max:
            raise ValueError('elev_max must be greater than elev_min')
        # Ensure the taper start and end coordinates are not outside the mesh
        if (not 0.0 < taper_start < length
                or not 0.0 < taper_end < length):
            raise ValueError('Taper must lie within the mesh length')
        # Ensure the taper is not inverted
        if taper_start >= taper_end:
            raise ValueError('taper_end must be greater than taper_start')
        # Ensure the taper ratio is not negative or zero
        if taper_ratio <= 0:
            raise ValueError('taper_ratio must be greater than zero')

        self.base_width = base_width
        self.length = length
        self.elev_min = elev_min
        self.elev_max = elev_max
        self.taper_start = taper_start
        self.taper_end = taper_end
        self.taper_ratio = taper_ratio

        self._scaling = 0.0
        self.domain = []

    def _cache_scaling(self):
        # Get the span of elevation values expected by the user
        range_z = self.elev_max - self.elev_min
        # Calculate the scaling factor to rescale the parabolae by. Since this
        # is only dependent on the target elevation scale and mesh width, this
        # factor is the same for all points in the mesh.
        self._scaling = (self.base_width/2)**2 / range_z

    def _calculate_domain(self):
        # This generates the list of lines defining the mesh boundary
        max_y_start = self.base_width/2
        max_y_end = self.base_width*self.taper_ratio/2
        domain_vertices = [
            # Starting edge
            (0.0, -max_y_start), (0.0, max_y_start),
            # Taper (positive y)
            (self.taper_start, max_y_start), (self.taper_end, max_y_end),
            # Outflow edge
            (self.length, max_y_end), (self.length, -max_y_end),
            # Taper (negative y)
            (self.taper_end, -max_y_end), (self.taper_start, -max_y_start)]

        # Convert point tuples to nodes
        self.nodes = [meshtool.Node(v) for v in domain_vertices]
        # Create the segments that define the mesh boundary
        self.segments = segments_from_perimeter(self.nodes)

    def elevation_at(self, point):
        x, y = point

        # First, wide section
        if x <= self.taper_start:
            elevation = y**2

        # Second, tapered section
        elif x < self.taper_end:
            # Get the relative X position along the taper
            rel = (x-self.taper_start) / (self.taper_end-self.taper_start)
            # Calculate the difference in elevation for this y over the taper
            diff = (y/self.taper_ratio)**2 - (y**2)
            # Interpolate the current elevation
            elevation = y**2 + diff*rel

        # Third, narrow section
        else:
            elevation = (y/self.taper_ratio)**2

        # Scale the elevation to match the target maximum elevation
        elevation /= self._scaling
        # Raise the mesh to match the given minimum elevation
        elevation += self.elev_min

        return elevation

    def build(self, max_area: float = -1.0, min_angle: float = 28.0,
              **kwargs: Any) -> Tuple[Mesh, Dict[str, List[int]]]:
        self._cache_scaling()
        self._calculate_domain()
        return super().build(max_area=max_area, min_angle=min_angle, **kwargs)


if __name__ == '__main__':
    # Add kwargs to overwrite your parameters as you please
    factory = ParabolaNarrowing(elev_min=10, elev_max=40)

    # Generate the mesh
    mesh, _ = factory.build(max_area=5, min_angle=28)

    # Save the mesh
    mesh.save('parabola_narrowing.2dm')
