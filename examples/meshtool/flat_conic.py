# type: ignore
# pylint: disable=invalid-name

from meshtool.factories import CircularMeshFactory

# "CircularMeshFactory" is a subclass that automatically generates an
# approximately circular mesh domain (i.e. boundary).
factory = CircularMeshFactory(radius=25.0, segments=36)


# Define the elevation function
@factory.set_elevation_func
def flat_conic(point):
    x, y = point
    return (x**2 + y**2/2) / 25


# Build and save the mesh
mesh, _ = factory.build(max_area=0.5)
mesh.save('flat_conic.2dm')
