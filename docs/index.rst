.. BASEmesh Python API documentation master file, created by
   sphinx-quickstart on Fri Sep 30 13:20:49 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:

   Documentation Home <self>

BASEmesh Python API Documentation
=================================

The following pages are automatically generated from the BASEmesh source code and document the user-facing API for BASEmesh.

This documentation is targeted at developers who wish to extend BASEmesh or use BASEmesh as a library in their own projects. As such, it is not intended to be a comprehensive guide to using BASEmesh. For that, please see the user documentation and tutorials in the `BASEmesh Wiki <https://gitlab.ethz.ch/vaw/public/basemesh-v2/-/wikis/home>`_.

.. toctree::
   :maxdepth: 1
   :caption: Basic Usage

   Mesh Generation <meshing>
   Mesh Interpolation <interpol>
   String Definitions <stringdefs>

.. toctree::
   :maxdepth: 1
   :caption: Advanced Usage

   BASEmesh Datatype <mesh>
   Triangle Wrapper <triangle>
   Abstract Class Interfaces <abc>

.. toctree::
   :maxdepth: 1
   :caption: Utilities

   Mesh Utilities <utils>
   Geometry Cleanup <cleanup>
   MeshTool <meshtool>

.. module:: basemesh

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
