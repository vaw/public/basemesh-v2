==================
String Definitions
==================

String definitions are used to define edges of nodes in the mesh along which boundary conditions or other model configuration settings may be specified.

This requires them to be defined along mesh edges, i.e. pairs of nodes which are shared by at least one element. Node strings that bisect an element are not allowed.

As a result, the BASEmesh plugin automatically includes any string definition lines in its meshing break liens to ensure a mesh edge is available for each string definition.

The string definition workflow is as follows:

1. Merge a copy of the string definition lines into the mesh break lines

2. Generate the quality mesh using these modified break lines

3. For each input string definition line, loop over the mesh nodes and retrieve the nodes that lie on it. Because the string definitions were part of the break lines layer, these nodes are guaranteed to be connected with mesh edges.

The first step must be performed by the user before running the meshing process. In the plugin, it is done as part of the geometry cleanup process by adding the string definition lines to the :class:`basemesh.PSLGBuilder` instance after the regular break lines.

Resolving the named string definitions to mesh node strings is performed by the :func:`basemesh.resolve_string_defs` function, documented below:

.. autofunction:: basemesh.resolve_string_defs

Output Formats
==============

Depending on the BASEMENT version used, string definitions are saved in a different format.

For BASEMENT 3, string definitions are stored in the 2DM file via the node string card. To support this, load the resulting string definitions into the :class:`basemesh.Mesh` instance after triangulation:

.. code-block:: python

   mesh = basemesh.Mesh()

   string_defs = resolve_string_defs(...)

   for name, nodes in string_defs.items():
      mesh.add_node_string(name, [mesh.get_node_by_id(i) for i in nodes])

After adding the node strings, they will be written automatically as part of the :meth:`basemesh.Mesh.save` call.

In BASEMENT 2, string definitions are stored in their own file. To support this, pass the resulting string definition to the :func:`basemesh.write_string_defs_sidecar` function, documented below:

.. autofunction:: basemesh.write_string_defs_sidecar

String Definition Length Limiting
=================================

Some version of BASEMENT have a limit on the number of nodes a string definition can have, e.g. 40 nodes in BASEMENT 3.

The recommended workaround for this is to split the string definition into multiple shorter strings. This can be done automatically by the :func:`basemesh.split_string_defs` function, documented below:

.. autofunction:: basemesh.split_string_defs

Dividing Constraints
====================

Some forms of internal boundary or coupling constraints require equal numbers of elements on both sides of the constraint. To help achieve this, the BASEmesh plugin provides dividing constraints on the break lines layer.

For example, setting a dividing constraint of 10 on a break line will cause it to be split into 10 equal length segments prior to meshing.

However, there is no mechanism in place to ensure that Triangle will not further subdivide these segments due to the chosen mesh quality constraints. As a result, the final mesh may have more than the expected 10 elements for this edge. The easiest way to confirm this is to visually inspect the mesh in QGIS or other viewers.

Due to the difficulty of ensuring that the given dividing constraint was not violated by Triangle's mesh refinement, this capability is not replicated in the BASEmesh Python API. Users who require it may manually subdivide their break lines prior to generating the Triangle input data.
