========
MeshTool
========

.. currentmodule:: basemesh.meshtool

The MeshTool module allows the generation of meshes with arbitrary elevation given by a user-defined elevation function.

Overview
========

The user provides a set of fixed vertices and break lines. BASEmesh's Triangle wrapper is then used to generate a 2D quality mesh from these input geometries.

MeshTool then iterates over all nodes and elements in the mesh, raising them to the elevation specified elevation function. This function must take in a tuple of x, y coordinates and return the corresponding elevation:

.. code-block:: python

   def elevation_function(point: Tuple[float, float]) -> float:
       """Example elevation function."""
       x, y = point
       return X**2 + y**2 / 10

Usage
=====

To get started with the ``meshtool`` module, import it from either the `basemesh` package or as a standalone module:

.. code-block:: python

   # Up to BASEmesh v2.1
   from basemesh import meshtool
   
   # BASEmesh v2.2+
   import meshtool

Next, a mesh factory must be specified that will generate the quality mesh. Basic factories for round and rectangular mesh domains are available in the :mod:`meshtool.factories` namespace. This example will instead use a custom mesh factory for illustrative purposes.

Mesh factories must inherit from :class:`meshtool.AbstractFactory`.
Create a new class inheriting from `AbstractFactory`. Be sure to call `super.__init__()` within your initializer to ensure the base class has been instantiated correctly.

.. code-block:: python

   class ExampleFactory(meshtool.AbstractFactory):
      def __init__(self):
         super().__init__()

Next, add any nodes and segments required to define the mesh boundary and break lines in the initializer. Feel free to add additional arguments to parametrize the mesh factory.

.. note::

   Be sure to enclose the outer boundary of the mesh with break lines as Triangle may otherwise "eat" all mesh elements after triangulation, resulting in an empty mesh.

.. code-block:: python

   class ExampleFactory(meshtool.AbstractFactory):
       def __init__(self):
           super().__init__()

           # Add nodes
           top_left = Node((-100, 100))
           top_right = Node((100, 100))
           bottom_left = Node((-100, -100))
           bottom_right = Node((100, -100))
           midpoint = Node((0, 0))
           self.nodes = [top_left, top_right, midpoint,
                         bottom_left, bottom_right]

           # Add segments
           top = Segment(top_left, top_right)
           left = Segment(top_left, bottom_left)
           right = Segment(top_right, bottom_right)
           bottom = Segment(bottom_left, bottom_right)
           self.segments = [top, left, right, bottom]
   
Now we must define the elevation function. This function should return a valid float for any point within the mesh boundary.

.. code-block:: python

   class ExampleFactory(meshtool.AbstractFactory):
       def __init__(self):
           (...)
   
       def elevation_at(self, point):
           # f(x, y) = (x² + y²/2) / 100
   
           x, y = point
           return (x**2 + y**2 / 2) / 100

With the boundary and elevation function specified, the mesh factory is ready to use. Outside the class body, instantiate the factory and run its :meth:`meshtool.AbstractFactory.build` method.

This method returns a tuple of the mesh itself and a mapping of string definitions. Save the mesh using the :meth:`basemesh.Mesh.save` method, then run your script.

.. code-block:: python

   my_factory = ExampleFactory()
   mesh, _ = my_factory.build(max_area=5, min_angle=30)
   mesh.save('example.2dm')

The resulting mesh will have node elevations set as required for BASEMENT 2. For BASEMENT 3, the mesh elements must be interpolated separately:

.. code-block:: python

   my_factory = ExampleFactory()
   mesh, _ = my_factory.build(max_area=5, min_angle=30)

   # Insert element elevations into the mesh file
   elevations = meshtool.calculate_element_elevation(mesh,    my_factory)
   for element in mesh.elements:
       element.materials = *element.materials, elevations[element.id]

   # Save the mesh
   mesh.save('example.2dm')

Here is the entire mesh generation script again for reference:

.. code-block:: python

   class ExampleFactory(meshtool.AbstractFactory):
       def __init__(self):
           super().__init__()
   
           # Add nodes
           top_left = Node(-100, 100)
           top_right = Node(100, 100)
           bottom_left = Node(-100, -100)
           bottom_right = Node(100, -100)
           midpoint = Node(0, 0)
           self.nodes = [top_left, top_right, midpoint,
                         bottom_left, bottom_right]
   
           # Add segments
           top = Segment(top_left, top_right)
           left = Segment(top_left, bottom_left)
           right = Segment(top_right, bottom_right)
           bottom = Segment(bottom_left, bottom_right)
           self.segments = [top, left, right, bottom]
   
       def elevation_at(self, point):
           # f(x, y) = (x² + y²/2) / 100
   
           x, y = point
           return (x**2 + y**2 / 2) / 100
   
   
   my_factory = ExampleFactory()
   mesh, _ = my_factory.build(max_area=5, min_angle=30)
   elevations = meshtool.calculate_element_elevation(mesh,    my_factory)
   for element in mesh.elements:
       element.materials = *element.materials, elevations   [element.id]
   mesh.save('example.2dm')

Mesh Regions & Holes
====================

Mesh regions and holes can be specified through :class:`basemesh.triangle.RegionMarker` and :class:`basemesh.triangle.HoleMarker` objects. The former controls the material ID and/or maximum cell area cosntraints, the latter removes a segment-bounded region from the output mesh.

These marker classes can be imported directly from the MeshTool namespace:

.. code-block:: python

   from meshtool import RegionMarker, HoleMarker

Both types of markers take the X and Y coordinate at which they are positioned. Lists of markers can be added to a given mesh factory using the :class:`regions <meshtool.AbstractFactory>` and :class:`holes <meshtool.AbstractFactory>` attributes.

.. code-block:: python

   my_factory = ExampleFactory()

   # Add mesh regions
   my_factory.regions = [
       RegionMarker(-50, -50, max_area=10, attribute=1),
       RegionMarker(50, 50, attribute=2)
   ]

   mesh, _ = my_factory.build(max_area=5, min_angle=30)
   elevations = meshtool.calculate_element_elevation(mesh, my_factory)
   for element in mesh.elements:
       element.materials = *element.materials, elevations[element.id]
   mesh.save('example_regions.2dm')

API Reference
=============

.. autoclass:: meshtool.AbstractFactory
   :members:

.. autoclass:: meshtool.Node
   :members:

Built-in Mesh Factories
=======================

.. module:: meshtool.factories

.. autoclass:: meshtool.BasicFactory
   :members:

.. autoclass:: meshtool.factories.CircularMeshFactory
   :members:

.. autoclass:: meshtool.factories.RectangularMeshFactory
   :members:
