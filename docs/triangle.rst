================
Triangle Wrapper
================

.. automodule:: basemesh.triangle

Basic Usage
===========

1. Define the input geometry to mesh using :class:`~basemesh.triangle.Node` and :class:`~basemesh.triangle.Segment` instances.

   .. note::

      When using :mod:`basemesh` and not the :mod:`~basemesh.triangle` module in isolation, you may also use the :class:`basemesh.PSLGBuilder` class to define the geometry. Call :meth:`~basemesh.PSLGBuilder.build` to get the list of nodes and segments to continue with the rest of the example.

2. Optionally define :class:`~basemesh.triangle.HoleMarker` and :class:`~basemesh.triangle.RegionMarker` instances to mark holes and segment-bounded regions in the geometry.

3. Optionally generate the Triangle command line arguments using the :func:`basemesh.triangle.command` helper function.

4. Call :func:`basemesh.triangle.triangulate` with the list of nodes, segments, optional hole or region markers, and command line arguments.

   This returns the file stem of the Triangle output files.

5. Finally, read the output files using :func:`basemesh.triangle.read_output`. This returns generators over the nodes and elements of the triangulation.

   .. note::

      When using :mod:`basemesh`, you can also use the :func:`basemesh.mesh_from_triangle` function to convert the Triangle output into a :class:`basemesh.Mesh` instance.

Triangle Data Representation
============================

.. autoclass:: basemesh.triangle.Node
   :members:

.. autoclass:: basemesh.triangle.Segment
   :members:

.. autoclass:: basemesh.triangle.HoleMarker
   :members:

.. autoclass:: basemesh.triangle.RegionMarker
   :members:

.. autoclass:: basemesh.triangle.Element
   :members:

Workflow
========

.. autofunction:: basemesh.triangle.command

.. autofunction:: basemesh.triangle.triangulate

.. autofunction:: basemesh.triangle.run_triangle

.. autofunction:: basemesh.triangle.read_output

.. autofunction:: basemesh.mesh_from_triangle
