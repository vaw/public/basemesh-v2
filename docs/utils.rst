=================
Meshing Utilities
=================

.. autofunction:: basemesh.utils.inherit_2dm_data

.. autofunction:: basemesh.utils.inject_matid

.. autofunction:: basemesh.utils.renumber_mesh
