================
Geometry Cleanup
================

Real-world GIS geometries are often not perfect, featuring self-intersections, duplicate vertices, or other problems. This makes them generally unsuitable for use in Triangle and other meshing tools. BASEmesh provides the :class:`~basemesh.PSLGBuilder` class to help clean up geometries.

Start by converting the input geometry into :class:`basemesh.Vertex` and :class:`basemesh.Segment` objects before loading them into a :class:`~basemesh.PSLGBuilder` instance.

Optionally, call the geometry cleanup methods on the :class:`~basemesh.PSLGBuilder` instance to resolve issues. Which methods to call, their precision settings, and the order in which they are called depend on the input geometry.

As of writing, the BASEmesh plugin uses the following cleanup process:

.. code-block:: python
   
   builder = PSLGBuilder()
   builder.add_vertices(...)  # Omitted for brevity
   builder.add_segments(...)  # Omitted for brevity

   tol = 1e-6  # "Snapping tolerance" setting
   
   builder.merge_vertices(tol)
   builder.merge_coincident_segments()
   builder.insert_intersection_vertices(tol)
   builder.split_segments(tol)
   builder.merge_vertices(tol)
   builder.remove_singular_segments()

The second call to :meth:`~basemesh.PSLGBuilder.merge_vertices` is required as splitting segments may have introduced new vertices that fall within the snapping tolerance of existing vertices.

Finally, call :meth:`~basemesh.PSLGBuilder.build` to create lists of nodes and segments that can be passed to Triangle.

Builder Object
==============

.. autoclass:: basemesh.PSLGBuilder
   :members:

Input Geometry
==============

.. autoclass:: basemesh.Vertex
   :members:

.. autoclass:: basemesh.Segment
   :members:
