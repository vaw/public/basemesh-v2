==================
Mesh Interpolation
==================

Quality meshes generated in BASEmesh have no elevation by default and must be interpolated to gain elevation information.

The interpolation system relies on the abstract :class:`basemesh.abc.ElevationSource` class interface, which is implemented by the :class:`basemesh.Mesh` class, among others.

When using custom elevation sources, the :class:`basemesh.abc.ElevationSource` interface must be adapted. The following example shows the adaptor used for QGIS DEM raster layers:

.. code-block:: python

   provider = layer.dataProvider()
   band_index = 1

   def elevation_at(self, point):
       value, ok = provider.sample(QgsPointXY(*point), band_index)
       if not ok or math.isnan(value):
           raise ValueError(f'Unable to interpolate point {point}')
       return float(value)

For BASEMENT 2, only the mesh nodes must be interpolated. For BASEMENT 3, the mesh elements must be interpolated instead. Interpolating both nodes and elements allows reusing the same mesh across multiple BASEMENT versions.

Node Elevation (BASEMENT 2)
===========================

.. autofunction:: basemesh.interpolate_mesh

Element Elevation (BASEMENT 3)
==============================

Mesh element elevations are not stored in the :class:`basemesh.Mesh` class as they do not update when the mesh is edited, instead acting like a snapshot of the mesh geometry at the time of interpolation.

The :func:`basemesh.calculate_element_elevation` function therefore returns a mapping of element IDs to elevations, which can be merged into the mesh attributes in a separate step:

.. code-block:: python

   mesh = Mesh.open(...)
   elevation_sources = [...]

   # Interpolate mesh elements
   element_elevations = basemesh.calculate_element_elevation(mesh, *elevation_sources)

   # Store element elevations in element materials
   for element in mesh.elements:
       bed_elev = elevations[element.id]
       matid = element.materials[0] if element.materials else 0
       element.materials = matid, bed_elev, *element.materials[1:]

   # Save mesh
   mesh.save(...)

.. autofunction:: basemesh.calculate_element_elevation
