=============
Mesh Datatype
=============

.. autoclass:: basemesh.Mesh
   :members:

.. autoclass:: basemesh.MeshNode
   :members:

.. autoclass:: basemesh.MeshElement
   :members:
