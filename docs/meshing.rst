===============
Mesh Generation
===============

All 2D mesh creation in BASEmesh is handled by the :mod:`basemesh.triangle` module. To simplify its use and to provide a consistent interface, BASEmesh wraps Triangle functionality in separate functions for elevation and quality mesh generation.

To gain access to Triangle input and output files, as well as full control over the Triangle command line, refer to the :mod:`basemesh.triangle` module documentation and the `Triangle documentation <https://www.cs.cmu.edu/~quake/triangle.html>`_.

Elevation Mesh Generation
=========================

Elevation meshes are basic triangulated irregular networks (TINs) that are generated from a set of points and/or break lines with elevation information.

The elevation meshing interface does not permit the user to specify custom meshing parameters as these are incompatible with the elevation meshing workflow in BASEmesh.

If your application requires convex hull meshing, set the ``keep_convex_hull`` keyword parameter to True. For more information on supported keyword parameters, see the :func:`basemesh.triangle.command` function documentation.

.. autofunction:: basemesh.elevation_mesh

Quality Mesh Generation
=======================

Quality meshes do not support elevation information and always generate a flat 2D mesh with all nodes at the default elevation of 0.0.

The quality meshing tool gives tiered control over mesh quality settings. Global settings for minimum included angle in generated elements and maximum element size are specified in the ``min_angle`` and ``max_area`` keyword parameters. These settings are applied to all elements in the mesh by default.

The global ``max_area`` setting may be overridden by the :class:`max_area <basemesh.triangle.RegionMarker>` field of a :class:`~basemesh.triangle.RegionMarker` instance and is only applied to elements within the same segment-bounded region as the marker. Triangle only supports the insertion of a single material ID per element based on a region marker's 
:class:`attribute <basemesh.triangle.RegionMarker>` field.

.. autofunction:: basemesh.quality_mesh
