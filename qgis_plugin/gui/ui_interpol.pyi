from PyQt5 import QtWidgets


class Ui_dialog:
    dialogLayout: QtWidgets.QVBoxLayout
    inputGroup: QtWidgets.QGroupBox
    verticalLayout_2: QtWidgets.QVBoxLayout
    layerRow: QtWidgets.QHBoxLayout
    layerLabel: QtWidgets.QLabel
    layer: QtWidgets.QComboBox
    tabWidget: QtWidgets.QTabWidget
    basicTab: QtWidgets.QWidget
    verticalLayout_5: QtWidgets.QVBoxLayout
    sourceGroup: QtWidgets.QGroupBox
    verticalLayout_6: QtWidgets.QVBoxLayout
    useMeshSource: QtWidgets.QRadioButton
    meshSourceRow: QtWidgets.QHBoxLayout
    meshSourceLabel: QtWidgets.QLabel
    meshSource: QtWidgets.QComboBox
    useRasterSource: QtWidgets.QRadioButton
    basicRasterGrid: QtWidgets.QGridLayout
    rasterSource: QtWidgets.QComboBox
    bandLabelRow: QtWidgets.QHBoxLayout
    bandLabel: QtWidgets.QLabel
    rasterLabelRow: QtWidgets.QHBoxLayout
    rasterLabel: QtWidgets.QLabel
    rasterBand: QtWidgets.QComboBox
    advancedTab: QtWidgets.QWidget
    verticalLayout_4: QtWidgets.QVBoxLayout
    listsGrid: QtWidgets.QGridLayout
    selectedLabel: QtWidgets.QLabel
    selectedList: QtWidgets.QListWidget
    availableLabel: QtWidgets.QLabel
    availableList: QtWidgets.QListWidget
    hintLabel: QtWidgets.QLabel
    helpTab: QtWidgets.QWidget
    verticalLayout_3: QtWidgets.QVBoxLayout
    help: QtWidgets.QTextEdit
    outputGroup: QtWidgets.QGroupBox
    verticalLayout_7: QtWidgets.QVBoxLayout
    formatRow: QtWidgets.QHBoxLayout
    formatLabel: QtWidgets.QLabel
    format: QtWidgets.QComboBox
    fileRow: QtWidgets.QHBoxLayout
    filePath: QtWidgets.QLineEdit
    fileBrowseBtn: QtWidgets.QPushButton
    optionsRow: QtWidgets.QHBoxLayout
    addToMap: QtWidgets.QCheckBox
    bottomBar: QtWidgets.QVBoxLayout
    progressBarRow: QtWidgets.QHBoxLayout
    progressBar: QtWidgets.QProgressBar
    cancelBtn: QtWidgets.QPushButton
    buttonRow: QtWidgets.QHBoxLayout
    status: QtWidgets.QLabel
    runBtn: QtWidgets.QPushButton
    closeBtn: QtWidgets.QPushButton

    def setupUi(self, dialog: QtWidgets.QDialog) -> None:
        ...
