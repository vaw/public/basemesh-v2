from PyQt5 import QtWidgets


class Ui_dialog:
    verticalLayout: QtWidgets.QVBoxLayout
    horizontalLayout: QtWidgets.QHBoxLayout
    banner: QtWidgets.QLabel
    textBrowser: QtWidgets.QTextBrowser
    buttonRow: QtWidgets.QHBoxLayout
    linkRepo: QtWidgets.QLabel
    linkWebsite: QtWidgets.QLabel
    closeBtn: QtWidgets.QPushButton

    def setupUi(self, dialog: QtWidgets.QDialog) -> None:
        ...
