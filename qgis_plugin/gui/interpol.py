# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Python implementation of the mesh interpolation GUI."""

import os
from typing import Any, List, Optional, Tuple, Union, cast

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidgetItem
from qgis.core import QgsMapLayerType, QgsMeshLayer, QgsProject, QgsRasterLayer
from qgis.gui import QgisInterface

from basemesh.abc import ElevationSource
from basemesh import Mesh, calculate_element_elevation, interpolate_mesh

from .._feedback import MultiStepFeedback
from .._raster import RasterDataWrapper
from ..utils import filename_from_mesh_layer

from ._base import DialogBase
from .ui_interpol import Ui_dialog as Ui_Interpol

__all__ = [
    'InterpolDialog',
]

# This is a custom type hint used to refer to mesh layers and tuples of a
# raster layer and its corresponding band index.
_ElevLayer = Union[QgsMeshLayer, Tuple[QgsRasterLayer, int]]


class InterpolDialog(DialogBase):
    """GUI implementaiton of the interpolation utility."""

    help_file = 'help_interpol.html'

    def __init__(self, iface: QgisInterface) -> None:
        """Connect signal callbacks and populate widgets."""
        super().__init__(iface)  # Initialize DialogBase
        self.ui: Ui_Interpol = Ui_Interpol()
        self.ui.setupUi(self)

        self._use_advanced = False
        self.load_help(self.ui.help)

        # Populate list widgets
        self._layers_cache: List[_ElevLayer] = []
        self._set_available_layers()  # Populate list widgets

        # Populate combo boxes
        self.add_mesh_layers(self.ui.layer, self.ui.meshSource)
        self.add_raster_layers(self.ui.rasterSource)

        # Hook up callbacks
        self.ui.tabWidget.currentChanged.connect(self._tab_switched)
        self.ui.rasterSource.currentIndexChanged.connect(
            self._update_raster_bands)
        self.ui.fileBrowseBtn.clicked.connect(
            self.browse_for_output(
                '2dm', self.ui.filePath, 'computational-mesh'))

        # Manually trigger some callbacks to pre-populate the combo boxes
        self.ui.rasterSource.currentIndexChanged.emit(0)

    def accept(self) -> None:
        """Evaluate the user's chosen settings and run the tool.

        This function runs when the user confirms their settings, its
        name may not be changed as it is tied directly into the default
        QDialog slot.
        """
        # Input layer
        target_layer = self.ui.layer.currentData()

        # NOTE: The following list contains the elevation sources to be used
        # for interpolation in descending order of priority.
        sources: List[ElevationSource] = []

        # Advanced mode
        if self._use_advanced:
            for list_item in self._get_list_selection():
                # Mesh layer
                if isinstance(list_item, QgsMeshLayer):
                    layer: QgsMeshLayer = list_item
                    filename = filename_from_mesh_layer(layer)
                    sources.append(Mesh.open(filename))
                # Raster layer
                else:
                    layer, band_index = list_item
                    sources.append(RasterDataWrapper(layer, band_index))

        # Basic mode
        else:
            # Use elevation mesh
            if self.ui.useMeshSource.isChecked():
                layer = self.ui.meshSource.currentData()
                filename = filename_from_mesh_layer(layer)
                sources = [Mesh.open(filename)]
            # Use raster data
            else:
                layer = self.ui.rasterSource.currentData()
                band_index = self.ui.rasterBand.currentData()
                sources = [RasterDataWrapper(layer, band_index)]

        # Output
        output_path = self.ui.filePath.text()
        format_value = self.ui.format.currentIndex()
        output_bm2 = output_bm3 = False
        if format_value != 1:
            output_bm2 = True
        if format_value >= 1:
            output_bm3 = True
        add_file_to_map = self.ui.addToMap.isChecked()

        def after(_: Any, error: Optional[BaseException]) -> None:
            """Run after the worker has finished.

            Parameters
            ----------
            _ : Any
                The worker's return value; discarded and unneeded
            error : Optional[BaseException]
                The error that forced the end of execution, by default
                None
            """
            # Add file to map
            if error is None and add_file_to_map:
                name, _ = tuple(os.path.basename(output_path).rsplit('.', 1))
                layer = QgsMeshLayer(output_path, name, 'mdal')
                QgsProject.instance().addMapLayer(layer)
            self._set_lockout(False)
            if error is not None:
                self._push_status((0.0, 'Error', False))
                self.print_error(error)

        self._set_lockout(True)
        self.run_worker(target_layer=target_layer, sources=sources,
                        output_path=output_path, output_bm2=output_bm2,
                        output_bm3=output_bm3, after=after,
                        cancel_btn=self.ui.cancelBtn)

    @staticmethod
    def worker(feedback: MultiStepFeedback, **kwargs: Any) -> None:
        """Perform the operation triggered by the dialog.

        This method is static as it will be carried out by a separate
        thread.

        Parameters
        ----------
        feedback : MultiStepFeedback
            The feedback object use to communicate with the parent
            thread; this will be auto-filled internally
        target_layer : QgsMeshLayer
            The mesh layer to interpolate
        sources : List[ElevationSource]
            The elevation sources to use for interpolation
        output_path : str
            The output path of the interpolated mesh
        output_bm2 : bool
            Whether to interpolate nodes (BASEMENT v2.8)
        output_bm3 : bool
            Whether to interpolate elements (BASEMENT v3.x)

        Raises
        ------
        ValueError
            Raised if neither output format is True
        """
        target_layer: QgsMeshLayer = kwargs.pop('target_layer')
        sources: List[ElevationSource] = kwargs.pop('sources')
        output_path: str = kwargs.pop('output_path')
        output_bm2: bool = kwargs.pop('output_bm2')
        output_bm3: bool = kwargs.pop('output_bm3')

        if not any((output_bm2, output_bm3)):
            raise ValueError('At least one output format required')

        feedback.setStepCount(int(output_bm2) + int(output_bm3))

        # Load input mesh file
        interpol_mesh = Mesh()
        feedback.setBusy(True)
        feedback.setLabel('Loading quality mesh...')
        filename = filename_from_mesh_layer(target_layer)
        interpol_mesh = Mesh.open(filename)
        feedback.setBusy(False)

        # Interpolate nodes
        feedback.setStep(1)
        if output_bm2:
            feedback.setProgress(0.0)
            feedback.setLabel('Interpolating mesh nodes...')
            interpolate_mesh(
                interpol_mesh, *sources,
                feedback=lambda p: feedback.setProgress(p*100))

        # Interpolate elements
        if output_bm3:
            if output_bm2:
                feedback.setStep(2)
            feedback.setProgress(0.0)
            feedback.setLabel('Interpolating mesh elements...')
            # Calculate element elevations
            elevations = calculate_element_elevation(
                interpol_mesh, *sources,
                feedback=lambda p: feedback.setProgress(p*100))

            # Update element materials
            for element in interpol_mesh.elements:
                bed_elev = elevations[element.id]
                # NOTE: QGIS always interprets the first material as the MATID
                # and the second as element bed elevation. If no MATID is
                # given, 0 is inserted to keep the dataset names in QGIS
                # consistent and avoid confusion for plugin users.
                matid = element.materials[0] if element.materials else 0
                element.materials = matid, bed_elev, *element.materials[1:]

        # Save output mesh file
        with feedback.busy('Saving computational mesh...'):
            interpol_mesh.save(output_path)

        feedback.setStepCount(0)
        feedback.setProgress(100.0)
        feedback.setLabel('Done')

    def _get_list_selection(self) -> List[_ElevLayer]:
        """Return the selected layers from the "Advanced" tab lists."""
        return_list: List[_ElevLayer] = []
        for index in range(self.ui.selectedList.count()):
            item = self.ui.selectedList.item(index)
            if item is not None:
                layer_index: int = item.data(Qt.UserRole)
                return_list.append(self._layers_cache[layer_index])
        return return_list

    def _set_available_layers(self) -> None:
        """Add all elevation sources to the available layers list."""
        # Clear the lists
        avail_list = self.ui.availableList
        avail_list.clear()
        self.ui.selectedList.clear()
        self._layers_cache = []
        # Add all mesh layers, as well as any raster bands
        for layer in self.iface.mapCanvas().layers():

            if layer.type() == QgsMapLayerType.MeshLayer:
                layer = cast(QgsMeshLayer, layer)

                if not layer.source().endswith('.2dm'):
                    continue  # NOTE: Only 2DM meshes supported for now

                new_item = QListWidgetItem(layer.name())
                new_item.setData(Qt.UserRole, len(self._layers_cache))
                self._layers_cache.append(layer)
                avail_list.addItem(new_item)

            elif layer.type() == QgsMapLayerType.RasterLayer:
                layer = cast(QgsRasterLayer, layer)

                for index in range(layer.bandCount()):
                    band_name = layer.bandName(index)
                    if not band_name:
                        band_name = f'Band #{index}'
                    new_item = QListWidgetItem(f'{layer.name()} • {band_name}')
                    new_item.setData(Qt.UserRole, len(self._layers_cache))
                    self._layers_cache.append((layer, index))
                    avail_list.addItem(new_item)

    def _set_lockout(self, is_locked: bool) -> None:
        """Lock out the GUI while running the tool.

        Parameters
        ----------
        is_locked : bool
            Whether to lock or unlock the GUI
        """
        self.ui.inputGroup.setEnabled(not is_locked)
        self.ui.tabWidget.setEnabled(not is_locked)
        self.ui.outputGroup.setEnabled(not is_locked)
        self.ui.closeBtn.setEnabled(not is_locked)
        self.ui.runBtn.setEnabled(not is_locked)
        self.ui.cancelBtn.setEnabled(is_locked)

    def _tab_switched(self, tab_index: int) -> None:
        """Switch between simple and advanced tabs.

        Parameters
        ----------
        tab_index : int
            The index of the tab that was switched to
        """
        # Basic tab
        if tab_index == 0 and self._use_advanced:
            self._use_advanced = False
        # Advanced tab
        elif tab_index == 1 and not self._use_advanced:
            self._use_advanced = True

    def _update_raster_bands(self) -> None:
        """Refresh the raster layer band combo box.

        This removes all items from the basic tab's raster band combo
        box and populates it with the bands found for the currently
        selected raster layer.
        """
        raster_layer = self.ui.rasterSource.currentData()
        band_box = self.ui.rasterBand
        band_box.clear()
        if raster_layer is None:
            return
        for index in range(raster_layer.bandCount()):
            band_name = raster_layer.bandName(index)
            if not band_name:
                band_name = f'Band #{index}'
            band_box.addItem(band_name)
            band_box.setItemData(band_box.count()-1, index)
