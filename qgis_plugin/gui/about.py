# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Python implementation of the about widget GUI."""

import os
from typing import Optional

from PyQt5.QtGui import QPixmap, QResizeEvent
from qgis.gui import QgisInterface

from ..utils import get_plugin_dir
from ._base import DialogBase
from .ui_about import Ui_dialog as Ui_About

__all__ = [
    'AboutDialog',
]


class AboutDialog(DialogBase):
    """GUI implementation of the elevation meshing utility."""

    def __init__(self, iface: QgisInterface) -> None:
        """Connect signal callbacks and populate widgets.

        Parameters
        ----------
        iface : QgisInterface
            The QGIS interface to create the dialog for

        """
        super().__init__(iface)  # Initialize DialogBase
        self.ui: Ui_About = Ui_About()
        self.ui.setupUi(self)

        # Load about text
        path = os.path.join(get_plugin_dir(), 'gui', 'about_basemesh.html')
        try:
            with open(path, encoding='utf-8') as file:
                text = file.read()
        except FileNotFoundError:
            text = f'Unable to set load HTML, file not found:\n{path}'
        self.ui.textBrowser.setHtml(text)

        # Apply size constraints
        self.pixmap = QPixmap(
            ':/plugins/basemesh_v2/icons/basemesh_logo_with_text.png')
        self.resizeEvent()

    def resizeEvent(self, a0: Optional[QResizeEvent] = None) -> None:
        """Event hook for the dialog being resized by the user."""
        # NOTE: Qt Designer does not support any form of "keep aspect ratio"
        # constraint, so this is set up in code instead.
        _ = a0
        scaled = self.pixmap.scaledToHeight(self.ui.banner.height())
        self.ui.banner.setPixmap(scaled)
