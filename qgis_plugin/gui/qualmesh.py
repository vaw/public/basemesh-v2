# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Python implementation of the quality meshing GUI."""

import logging
import os
from typing import Any, Dict, Iterable, List, Optional

from PyQt5.QtGui import QFont
from qgis.core import (QgsField, QgsGeometry, QgsMeshLayer, QgsPointXY,
                       QgsProject, QgsVectorLayer)
from qgis.gui import QgisInterface

from basemesh import (Mesh, MeshElement, MeshNode, PSLGBuilder, Segment,
                      Vertex, quality_mesh, split_string_defs,
                      write_string_defs_sidecar)
from basemesh.triangle import HoleMarker, RegionMarker
from basemesh.types import Line2D, Point2D

from .._feedback import Feedback, MultiStepFeedback
from ..utils import (extract_lines, extract_points, extract_polygons,
                     lines_from_polygons, process_markers, process_string_defs)
from ._base import DialogBase
from .ui_qualmesh import Ui_dialog as Ui_QualMesh

__all__ = [
    'QualMeshDialog'
]

# NOTE: If the font "Courier New" is unavailable, Qt will pick a different
# monospace font based on the style hint.
_monospace_font = QFont('Courier New')
_monospace_font.setStyleHint(QFont.Monospace)


class QualMeshDialog(DialogBase):
    """GUI implementation of the quality meshing utility."""

    help_file = 'help_qualmesh.html'

    def __init__(self, iface: QgisInterface) -> None:
        """Connect signal callbacks and populate widgets."""
        super().__init__(iface)  # Initialize DialogBase
        self.ui: Ui_QualMesh = Ui_QualMesh()
        self.ui.setupUi(self)

        self.ui.log.setFont(_monospace_font)
        self.load_help(self.ui.help)

        # Populate combo boxes
        self.ui.linesLayer.clear()
        self.ui.pointsLayer.clear()
        self.ui.regionsLayer.clear()
        self.ui.stringDefLayer.clear()
        self.ui.boundaryLayer.clear()
        self.add_point_layers(self.ui.regionsLayer, self.ui.pointsLayer)
        self.add_polyline_layers(self.ui.linesLayer, self.ui.stringDefLayer)
        self.add_polygon_layers(self.ui.boundaryLayer)

        # Hook up callbacks
        self.ui.linesLayer.currentIndexChanged.connect(
            self._update_dividing_constraint)
        self.ui.stringDefLayer.currentIndexChanged.connect(
            self._update_stringdef_field)
        self.ui.saveLogBtn.clicked.connect(
            self.export_log('qualmesh', self.ui.log))
        self.ui.regionsLayer.currentIndexChanged.connect(
            self._update_region_attributes)
        self.ui.stringDefBrowseBtn.clicked.connect(
            self.browse_for_output('txt', self.ui.stringDefPath,
                                   'string-defs'))
        self.ui.fileBrowseBtn.clicked.connect(
            self.browse_for_output('2dm', self.ui.filePath, 'quality-mesh'))

        # Manually trigger some callbacks to pre-populate the combo boxes
        self.ui.linesLayer.currentIndexChanged.emit(0)
        self.ui.regionsLayer.currentIndexChanged.emit(0)
        self.ui.stringDefLayer.currentIndexChanged.emit(0)

    def accept(self) -> None:
        """Evaluate the user's chosen settings and run the tool.

        This function runs when the user confirms their settings, its
        name may not be changed as it is tied directly into the default
        QDialog slot.
        """
        kwargs: Dict[str, Any] = {}

        # Input geometry
        points_data = self.ui.pointsLayer.currentData()
        if self.ui.usePoints.isChecked() and points_data is not None:
            points = list(extract_points(points_data, use_z=False))
        else:
            points = []
        lines_data = self.ui.linesLayer.currentData()
        if self.ui.useLines.isChecked() and lines_data is not None:
            divcon_field: Optional[QgsField] = None
            if self.ui.useDivCons.isChecked():
                divcon_field = self.ui.divConsField.currentData()
            lines = list(extract_lines(lines_data, use_z=False,
                                       divcon_field=divcon_field))
        else:
            lines = []

        # Mesh domain
        boundary: List[QgsGeometry] = []
        if self.ui.shrinkToSegments.isChecked():
            pass
        elif self.ui.customBoundary.isChecked():
            boundary = extract_polygons(self.ui.boundaryLayer.currentData())
            lines.extend(lines_from_polygons(boundary))
        else:
            raise RuntimeError('No mesh domain specified')

        # Region markers
        region_markers: List[RegionMarker] = []
        hole_markers: List[HoleMarker] = []
        if self.ui.regionsGroup.isChecked():
            markers_layer = self.ui.regionsLayer.currentData()
            reg = {}
            if self.ui.useMaxArea.isChecked():
                reg['area_field'] = self.ui.maxAreaField.currentData()
            if self.ui.useMatid.isChecked():
                reg['matid_field'] = self.ui.matidField.currentData()
            if self.ui.useHole.isChecked():
                reg['hole_field'] = self.ui.holeField.currentData()
            region_markers, hole_markers = process_markers(
                markers_layer, **reg)

        # String definitions
        stringdef_layer = self.ui.stringDefLayer.currentData()
        if self.ui.stringDefGroup.isChecked() and stringdef_layer is not None:
            stringdef_field = self.ui.stringDefField.currentData()
            lines.extend(extract_lines(stringdef_layer, use_z=False))
        else:
            stringdef_field = None
        stringdef_limit = 0
        if self.ui.limitStringDefs.isChecked():
            if self.ui.maxStringDefLength.value() > 0:
                stringdef_limit = int(self.ui.maxStringDefLength.value())

        # Precision
        precision = -1.0
        if not self.ui.disableCleaning.isChecked():
            precision = 0.0
            if self.ui.enableSnapping.isChecked():
                precision = float(10 ** self.ui.snappingTolerance.value())

        # Parameters
        max_area: float = (self.ui.globalMaxArea.value()
                           if self.ui.useGlobalMaxArea.isChecked() else -1.0)
        min_angle: float = (self.ui.globalMinAngle.value()
                            if self.ui.useGlobalMinAngle.isChecked() else -1.0)
        kwargs['custom_args'] = (self.ui.customArgs.text()
                                 if self.ui.useCustomArgs.isChecked() else '')
        log_level = self.ui.logLevel.currentIndex()
        # NOTE: 0 -> Basic, 1 -> Advanced, 2-4 -> Debug 1-3
        if log_level > 0:
            kwargs['debug_level'] = log_level
            if log_level > 1:
                logging.getLogger('triangle').setLevel(logging.DEBUG)

        # Output
        output_path = self.ui.filePath.text()
        if self.ui.keepTempFiles.isChecked():
            kwargs['move_tempfiles'] = os.path.dirname(output_path)
        else:
            kwargs['move_tempfiles'] = None
        add_file_to_map = self.ui.addToMap.isChecked()

        if min_angle > 0.0:
            kwargs['min_angle'] = min_angle
        if max_area > 0.0:
            kwargs['max_area'] = max_area

        def after(_: Any, error: Optional[BaseException]) -> None:
            """Run after the worker has finished.

            Parameters
            ----------
            _ : Any
                The worker's return value; discarded and unneeded
            error : Optional[BaseException]
                The error that forced the end of execution, by default
                None
            """
            if error is None and add_file_to_map:
                layer_name = os.path.basename(output_path).rsplit('.', 1)[0]
                layer = QgsMeshLayer(output_path, layer_name, 'mdal')
                QgsProject.instance().addMapLayer(layer)
            self._set_lockout(False)
            if error is not None:
                self._push_status((0.0, 'Error', False))
                self.print_error(error)

        self._set_lockout(True)
        if not self.ui.stringDefGroup.isChecked():
            stringdef_layer = None
        self.run_worker(precision=precision, points=points, lines=lines,
                        boundary=boundary, filename=output_path,
                        stringdefs_layer=stringdef_layer,
                        stringdefs_field=stringdef_field, holes=hole_markers,
                        regions=region_markers,
                        stringdef_limit=stringdef_limit,
                        sd_mesh=self.ui.includeInMesh.isChecked(),
                        sd_sidecar=self.ui.writeToSidecar.isChecked(),
                        sidecar_file=self.ui.stringDefPath.text(),
                        log=self.ui.log, after=after,
                        cancel_btn=self.ui.cancelBtn, **kwargs)

    @staticmethod
    def worker(feedback: MultiStepFeedback, **kwargs: Any) -> None:
        """Perform the operation triggered by the dialog.

        This method is static as it will be carried out by a separate
        thread.

        Parameters
        ----------
        feedback : MultiStepFeedback
            The feedback object use to communicate with the parent
            thread; this will be auto-filled internally
        precision : float
            The precision to use when generating the lattice
        points : Iterable[Tuple[float, float]]
            Input points to triangulate
        lines : Iterable[Tuple[Tuple[float, float],
                         Tuple[float, float]]]
            Input break lines to triangulate
        boundary : List[QgsPolygonXY]
            Input boundary polygons; if not empty, any elements outside
            of these polygons will be removed
        filename : str
            The output path of the generated mesh
        stringdefs_layer : Optional[QgsVectorLayer]
            The layer containing string definitions
        stringdefs_field : Optional[QgsField]
            The field of the stringdefs layer to use as an identifier
        holes : List[HoleMarker]
            Any hole markers found
        regions : List[RegionMarker]
            Any region markers found
        sd_mesh : bool
            Whether to write string definitions into the mesh itself
        sd_sidecar : bool
            Whether to write string definitions into a sidecare file
        sidecar_file : str
            The name of the sidecar file to write string definitions
            into
        """

        precision: float = kwargs.pop('precision')
        points: Iterable[Point2D] = kwargs.pop('points')
        lines: Iterable[Line2D] = kwargs.pop('lines')
        boundary: List[QgsGeometry] = kwargs.pop('boundary')
        filename: str = kwargs.pop('filename')
        stringdefs_layer: Optional[QgsVectorLayer] = (
            kwargs.pop('stringdefs_layer'))
        stringdefs_field: Optional[QgsField] = (
            kwargs.pop('stringdefs_field'))
        holes: List[HoleMarker] = kwargs.pop('holes')
        regions: List[RegionMarker] = kwargs.pop('regions')
        stringdef_limit: int = kwargs.pop('stringdef_limit')
        sd_mesh: bool = kwargs.pop('sd_mesh')
        sd_sidecar: bool = kwargs.pop('sd_sidecar')
        sidecar_file: str = kwargs.pop('sidecar_file')

        def process_input_data(precision: float, points: Iterable[Point2D],
                               lines: Iterable[Line2D], feedback: Feedback
                               ) -> PSLGBuilder:
            """Consolidate the given input geometry into a lattice.

            Parameters
            ----------
            precision : float
                The precision to use when generating the lattice
            points : Iterable[Tuple[float, float]]
                Points to add to the lattice
            lines : Iterable[Tuple[float, float], Tuple[float, float]]
                Lines to add to the lattice
            feedback : Feedback
                The feedback object used to communicate with the parent
                thread

            Returns
            -------
            PSLGBuilder
                A build populated with the processed input data
            """
            # Instantiate lattice. This object will hold and process all of the
            # input geometries.
            builder = PSLGBuilder()

            # Load input points into lattice
            with feedback.busy('Adding geometries to lattice...'):
                print('Reading points...')
                builder.add_vertices(Vertex((*p, 0.0)) for p in points)
                # Load input line segments into lattice
                print('Reading break lines...')
                for line in lines:
                    vertices = [Vertex((*p, 0.0)) for p in line]
                    builder.add_vertices(vertices)
                    builder.add_segment(Segment(*vertices))

            # Conform input data. This removes duplicates, splits segments, etc.
            if precision < 0.0:
                print('Input cleaning disabled')
            else:
                feedback.setBusy(True)
                feedback.setLabel('Cleaning input geometries...')
                merged = builder.merge_vertices(tol=precision)
                print(f'  {merged} duplicate vertices merged')
                merged = builder.merge_coincident_segments()
                print(f'  {merged} coincident segments merged')
                inserted = builder.insert_intersection_vertices(tol=precision)
                print(f'  {inserted} vertices inserted at intersections')
                split = builder.split_segments(tol=precision)
                print(f'  {split} segments split at intersections')
                merged = builder.merge_vertices(tol=precision)
                print(f'  {merged} duplicate vertices merged')
                removed = builder.remove_singular_segments()
                print(f'  {removed} zero-length segments removed')
                feedback.setBusy(False)

            return builder

        feedback.setStepCount(2 if stringdefs_layer is None else 3)

        print('Processing input geometries\n---------------------------')
        feedback.setStep(1)
        builder = process_input_data(precision, points, lines, feedback)

        print('\n\nRunning Triangle\n----------------\n')
        feedback.setStep(2)
        feedback.setBusy(True)
        feedback.setLabel('Running Triangle')
        mesh = quality_mesh(*builder.build(), holes, regions, **kwargs)
        feedback.setBusy(False)

        # Delete any elements outside of the boundary polygons
        if boundary:
            print('Removing elements outside of boundary polygons...')
            to_remove: List[MeshElement] = []
            for polygon in boundary:
                for element in mesh.elements:
                    if not polygon.contains(
                            QgsPointXY(*element.spatial_marker)):
                        to_remove.append(element)
            for element in to_remove:
                mesh.remove_element(element)
            print(f'  {len(to_remove)} elements removed')
            print(f'  {len(mesh.purge_nodes())} disjoint nodes removed')
            
            # Renumber mesh
            old_mesh = mesh
            mesh = Mesh()
            
            node_id_map: Dict[int, MeshNode]= {}
            for index, node in enumerate(old_mesh.nodes):
                node_id_map[node.id] = mesh.add_node(node.pos, index + 1)
            for index, element in enumerate(old_mesh.elements):
                element_nodes = tuple(node_id_map[n.id] for n in element.nodes)
                mesh.add_element(element_nodes, index + 1, *element.materials)
    
            print(f'  The mesh has been renumbered to fill ID range gaps')

        if stringdefs_layer is not None:
            print('\n\nProcessing string definitions\n'
                  '-----------------------------\n')
            feedback.setStep(3)
            feedback.setLabel('Processing string definitions...')
            string_defs = process_string_defs(
                stringdefs_layer, stringdefs_field, mesh, precision,
                feedback=feedback)
            input_count = len(string_defs)
            print(f'Found {input_count} string definitions')
            string_defs = split_string_defs(string_defs, stringdef_limit)
            if len(string_defs) != input_count:
                print(f'  {len(string_defs)} string definitions after '
                      f'splitting (limit={stringdef_limit})')
            if sd_mesh:
                print('Writing node strings to mesh file...')
                for name, nodes in string_defs.items():
                    mesh.add_node_string(
                        name, [mesh.get_node_by_id(i) for i in nodes])
            if sd_sidecar:
                print('Writing node strings to sidecar file...')
                write_string_defs_sidecar(string_defs, sidecar_file)

        mesh.save(filename)

        feedback.setStepCount(0)
        feedback.setProgress(100.0)
        feedback.setLabel('Done')

    def _set_lockout(self, is_locked: bool) -> None:
        """Lock out the GUI while running the tool.

        Parameters
        ----------
        is_locked : bool
            Whether to lock or unlock the GUI
        """
        self.ui.constraintsGroup.setEnabled(not is_locked)
        self.ui.regionsGroup.setEnabled(not is_locked)
        self.ui.meshDomainGroup.setEnabled(not is_locked)
        self.ui.stringDefGroup.setEnabled(not is_locked)
        self.ui.settingsGroup.setEnabled(not is_locked)
        self.ui.outputGroup.setEnabled(not is_locked)
        self.ui.runBtn.setEnabled(not is_locked)
        self.ui.closeBtn.setEnabled(not is_locked)
        self.ui.cancelBtn.setEnabled(is_locked)

    def _update_dividing_constraint(self) -> None:
        """Refresh the dividing constraints attribute combo box."""
        breaklines_layer = self.ui.linesLayer.currentData()
        divcon_box = self.ui.divConsField
        divcon_box.clear()
        if breaklines_layer is None:
            return
        for field in breaklines_layer.dataProvider().fields():
            if field.isNumeric() and field.precision() == 0:
                divcon_box.addItem(field.name())
                divcon_box.setItemData(divcon_box.count()-1, field)

    def _update_region_attributes(self) -> None:
        """Refresh the region attribute combo boxes."""
        regions_layer = self.ui.regionsLayer.currentData()
        area_box = self.ui.maxAreaField
        hole_box = self.ui.holeField
        matid_box = self.ui.matidField
        # Clear boxes
        area_box.clear()
        hole_box.clear()
        matid_box.clear()
        if regions_layer is None:
            return
        # Process fields
        for field in regions_layer.dataProvider().fields():
            if not field.isNumeric():
                continue
            area_box.addItem(field.name())
            area_box.setItemData(area_box.count()-1, field)
            if field.precision() == 0:
                hole_box.addItem(field.name())
                hole_box.setItemData(hole_box.count()-1, field)
                matid_box.addItem(field.name())
                matid_box.setItemData(matid_box.count()-1, field)

    def _update_stringdef_field(self) -> None:
        """Refresh the string def name attribute combo box."""
        stringdefs_layer = self.ui.stringDefLayer.currentData()
        sd_box = self.ui.stringDefField
        sd_box.clear()
        if stringdefs_layer is None:
            return
        for field in stringdefs_layer.dataProvider().fields():
            if not field.isNumeric():
                sd_box.addItem(field.name())
                sd_box.setItemData(sd_box.count()-1, field)
