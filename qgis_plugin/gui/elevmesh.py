# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Python implementation of the elevation meshing GUI."""

import os
from typing import Any, Dict, Iterable, Optional

from PyQt5.QtGui import QFont
from qgis.core import QgsMeshLayer, QgsProject
from qgis.gui import QgisInterface

from basemesh import PSLGBuilder, Segment, Vertex, elevation_mesh
from basemesh.types import Line3D, Point3D

from .._feedback import Feedback, MultiStepFeedback
from ..utils import extract_lines, extract_points

from ._base import DialogBase
from .ui_elevmesh import Ui_dialog as Ui_ElevMesh

__all__ = [
    'ElevMeshDialog',
]

# NOTE: If the font "Courier New" is unavailable, Qt will pick a different
# monospace font based on the style hint.
_monospace_font = QFont('Courier New')
_monospace_font.setStyleHint(QFont.Monospace)


class ElevMeshDialog(DialogBase):
    """GUI implementation of the elevation meshing utility."""

    help_file = 'help_elevmesh.html'

    def __init__(self, iface: QgisInterface) -> None:
        """Connect signal callbacks and populate widgets.

        Parameters
        ----------
        iface : QgisInterface
            The QGIS interface to create the dialog for
        """
        super().__init__(iface)  # Initialize DialogBase
        self.ui: Ui_ElevMesh = Ui_ElevMesh()
        self.ui.setupUi(self)

        self.ui.log.setFont(_monospace_font)
        self.load_help(self.ui.help)
        self.ui.status.setText('Ready')

        # Populate combo boxes
        self.ui.linesLayer.clear()
        self.ui.pointsLayer.clear()
        self.ui.boundaryLayer.clear()
        self.add_polyline_layers(self.ui.linesLayer, has_z=True)
        self.add_point_layers(self.ui.pointsLayer, has_z=True)
        self.add_polygon_layers(self.ui.boundaryLayer)

        # Hook up callbacks
        self.ui.fileBrowseBtn.clicked.connect(
            self.browse_for_output('2dm', self.ui.filePath, 'elevation-mesh'))
        self.ui.saveLogBtn.clicked.connect(
            self.export_log('elevmesh', self.ui.log))

        # Disabled features
        self.ui.customBoundary.setEnabled(False)

    def accept(self) -> None:
        """Evaluate the user's chosen settings and run the tool.

        This function runs when the user confirms their settings, its
        name may not be changed as it is tied directly into the default
        QDialog slot.
        """
        kwargs: Dict[str, Any] = {}

        # Input geometry
        lines_data = self.ui.linesLayer.currentData()
        if self.ui.useLines.isChecked() and lines_data is not None:
            lines = list(extract_lines(lines_data))
        else:
            lines = []
        points_data = self.ui.pointsLayer.currentData()
        if self.ui.usePoints.isChecked() and points_data is not None:
            points = list(extract_points(points_data))
        else:
            points = []

        # Mesh domain
        if self.ui.keepConvexHull.isChecked():
            kwargs['keep_convex_hull'] = True
        # Settings
        log_level = self.ui.logLevel.currentIndex()
        kwargs['debug_level'] = log_level
        precision = -1.0
        if not self.ui.disableCleaning.isChecked():
            precision = 0.0
            if self.ui.enableSnapping.isChecked():
                precision = float(10 ** self.ui.snappingTolerance.value())
        # Output
        filename = self.ui.filePath.text()
        if self.ui.keepTempFiles.isChecked():
            kwargs['move_tempfiles'] = os.path.dirname(filename)
        add_to_map = self.ui.addToMap.isChecked()

        def after(_: Any, error: Optional[BaseException]) -> None:
            """Run after the worker has finished.

            Parameters
            ----------
            _ : Any
                The worker's return value; discarded and unneeded
            error : Optional[BaseException]
                The error that forced the end of execution, by default
                None
            """
            if error is None and add_to_map:
                layer_name = os.path.basename(filename).rsplit('.', 1)[0]
                layer = QgsMeshLayer(filename, layer_name, 'mdal')
                QgsProject.instance().addMapLayer(layer)
            self._set_lockout(False)
            if error is not None:
                self._push_status((0.0, 'Error', False))
                self.print_error(error)

        self._set_lockout(True)
        self.run_worker(precision=precision, points=points, lines=lines,
                        filename=filename, log=self.ui.log, after=after,
                        cancel_btn=self.ui.cancelBtn, **kwargs)

    @staticmethod
    def worker(feedback: MultiStepFeedback, **kwargs: Any) -> None:
        """Perform the actual work of this utility.

        Any keyword arguments are passed on to the elevation_mesh
        function.

        Parameters
        ----------
        feedback : MultiStepFeedback
            The feedback object use to communicate with the parent
            thread; this will be auto-filled internally
        precision : float
            The precision to use when generating the lattice
        points : Iterable[Tuple[float, float, float]]
            Input points to triangulate
        lines : Iterable[Tuple[float, float, float],
                         Tuple[float, float, float]]
            Input break lines to triangulate
        filename : str
            The output path of the generated mesh
        """
        precision: float = kwargs.pop('precision')
        points: Iterable[Point3D] = kwargs.pop('points')
        lines: Iterable[Line3D] = kwargs.pop('lines')
        filename: str = kwargs.pop('filename')

        def process_input_data(precision: float, points: Iterable[Point3D],
                               lines: Iterable[Line3D], feedback: Feedback
                               ) -> PSLGBuilder:
            """Consolidate the given input geometry into a lattice.

            Parameters
            ----------
            precision : float
                The precision to use when generating the lattice
            points : Iterable[Tuple[float, float, float]]
                Points to add to the lattice
            lines : Iterable[Tuple[float, float, float],
                            Tuple[float, float, float]]
                Lines to add to the lattice
            feedback : Feedback
                The feedback object used to communicate with the parent
                thread

            Returns
            -------
            PSLGBuilder
                A build populated with the processed input data
            """
            # Instantiate lattice. This object will hold and process all of the
            # input geometries.
            builder = PSLGBuilder()

            # Load input points into lattice
            feedback.setBusy(True)
            feedback.setLabel('Adding geometries to lattice...')
            print('Reading points...')
            for point in points:
                builder.add_vertex(Vertex(point))
            # Load input line segments into lattice
            print('Reading break lines...')
            for line in lines:
                vertices = list(map(Vertex, line))
                builder.add_vertices(vertices)
                builder.add_segment(Segment(vertices[0], vertices[1]))
            feedback.setBusy(False)

            # Conform input data. This removes duplicates, splits segments, etc.
            if precision < 0.0:
                print('Input cleaning disabled')
            else:
                print('Cleaning input geometries...')
                merged = builder.merge_vertices(tol=precision)
                print(f'  {merged} duplicate vertices merged')
                merged = builder.merge_coincident_segments()
                print(f'  {merged} coincident segments merged')
                inserted = builder.insert_intersection_vertices(tol=precision)
                print(f'  {inserted} vertices inserted at segment intersections')
                split = builder.split_segments(tol=precision)
                print(f'  {split} segments split at intersections')
                merged = builder.merge_vertices(tol=precision)
                print(f'  {merged} duplicate vertices merged')
                removed = builder.remove_singular_segments()
                print(f'  {removed} zero-length segments removed')

            return builder

        feedback.setStepCount(2)

        print('Processing input geometries\n---------------------------')
        feedback.setStep(1)
        builder = process_input_data(precision, points, lines, feedback)

        print('\n\nRunning Triangle\n----------------\n')
        feedback.setStep(2)
        with feedback.busy('Generating elevation mesh...'):
            mesh = elevation_mesh(*builder.build(), **kwargs)
            mesh.save(filename, num_materials=0)

        feedback.setStepCount(0)
        feedback.setProgress(100.0)
        feedback.setLabel('Done')

    def _set_lockout(self, is_locked: bool) -> None:
        """Lock out the GUI while running the tool.

        Parameters
        ----------
        is_locked : bool
            Whether to lock or unlock the GUI
        """
        self.ui.constraintsGroup.setEnabled(not is_locked)
        self.ui.meshDomainGroup.setEnabled(not is_locked)
        self.ui.settingsGroup.setEnabled(not is_locked)
        self.ui.outputGroup.setEnabled(not is_locked)
        self.ui.closeBtn.setEnabled(not is_locked)
        self.ui.runBtn.setEnabled(not is_locked)
        self.ui.cancelBtn.setEnabled(is_locked)
