from PyQt5 import QtWidgets


class Ui_dialog:
    dialogLayout: QtWidgets.QVBoxLayout
    splitPanelContainer: QtWidgets.QHBoxLayout
    leftPanel: QtWidgets.QVBoxLayout
    constraintsGroup: QtWidgets.QGroupBox
    constraintsGroupLayout: QtWidgets.QVBoxLayout
    constraintsGrid: QtWidgets.QGridLayout
    linesLayer: QtWidgets.QComboBox
    useLines: QtWidgets.QCheckBox
    usePoints: QtWidgets.QCheckBox
    pointsLayer: QtWidgets.QComboBox
    aboutConstraints: QtWidgets.QLabel
    meshDomainGroup: QtWidgets.QGroupBox
    meshDomainGroupLayout: QtWidgets.QVBoxLayout
    keepConvexHull: QtWidgets.QRadioButton
    shrinkToSegments: QtWidgets.QRadioButton
    customBoundaryRow: QtWidgets.QHBoxLayout
    customBoundary: QtWidgets.QRadioButton
    boundaryLayer: QtWidgets.QComboBox
    aboutDomain: QtWidgets.QLabel
    settingsGroup: QtWidgets.QGroupBox
    settingsGroupLayout: QtWidgets.QGridLayout
    logLevel: QtWidgets.QComboBox
    enableSnapping: QtWidgets.QCheckBox
    disableCleaning: QtWidgets.QCheckBox
    logLevelLabel: QtWidgets.QLabel
    snappingLayout: QtWidgets.QHBoxLayout
    snappingTolerance: QtWidgets.QSpinBox
    outputGroup: QtWidgets.QGroupBox
    outputGroupLayout: QtWidgets.QVBoxLayout
    fileRow: QtWidgets.QHBoxLayout
    filePath: QtWidgets.QLineEdit
    fileBrowseBtn: QtWidgets.QPushButton
    optionsRow: QtWidgets.QHBoxLayout
    keepTempFiles: QtWidgets.QCheckBox
    addToMap: QtWidgets.QCheckBox
    tabWidget: QtWidgets.QTabWidget
    logTab: QtWidgets.QWidget
    logTabLayout: QtWidgets.QVBoxLayout
    log: QtWidgets.QPlainTextEdit
    exportRow: QtWidgets.QHBoxLayout
    saveLogBtn: QtWidgets.QPushButton
    clearLogBtn: QtWidgets.QPushButton
    helpTab: QtWidgets.QWidget
    helpTabLayout: QtWidgets.QVBoxLayout
    help: QtWidgets.QTextEdit
    bottomBar: QtWidgets.QVBoxLayout
    progressBarRow: QtWidgets.QHBoxLayout
    progressBar: QtWidgets.QProgressBar
    cancelBtn: QtWidgets.QPushButton
    buttonRow: QtWidgets.QHBoxLayout
    status: QtWidgets.QLabel
    runBtn: QtWidgets.QPushButton
    closeBtn: QtWidgets.QPushButton

    def setupUi(self, dialog: QtWidgets.QDialog) -> None:
        ...
