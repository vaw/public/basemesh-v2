# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Algorithms allowing the retrieval of elements by location."""

from typing import Any, Dict, List, Optional

from PyQt5.QtCore import QVariant
from qgis.core import (QgsFeature, QgsField, QgsFields, QgsMeshLayer, QgsPoint,
                       QgsProcessing, QgsProcessingContext,
                       QgsProcessingFeedback,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterMeshLayer,
                       QgsProcessingParameterVectorLayer, QgsVectorLayer,
                       QgsWkbTypes)

from basemesh import Mesh
from basemesh.types import Point2D

from ..utils import filename_from_mesh_layer

from ._base import BaseMeshProcessingAlgorithm

__all__ = [
    'GetMeshNodeByLocation',
    'GetMeshElementByLocation',
]


class GetMeshNodeByLocation(BaseMeshProcessingAlgorithm):
    """Retrieve the IDs of the closest nodes for a set of points."""

    INPUT = 'INPUT'
    POINTS = 'POINTS'
    OUTPUT = 'OUTPUT'

    def displayName(self) -> str:
        return 'Get mesh node by location'

    def initAlgorithm(
            self, configuration: Optional[Dict[str, Any]] = None) -> None:

        _ = configuration
        # Input mesh
        self.addParameter(QgsProcessingParameterMeshLayer(
            name=self.INPUT,
            description='Mesh layer'))
        # Input points
        self.addParameter(QgsProcessingParameterVectorLayer(
            name=self.POINTS,
            description='Input points',
            types=[QgsProcessing.SourceType.TypeVectorPoint]))
        # Output
        self.addParameter(QgsProcessingParameterFeatureSink(
            name=self.OUTPUT,
            description='Output point layer',
            type=QgsProcessing.SourceType.TypeVectorPoint))

    def processAlgorithm(self, parameters: Dict[str, Any],
                         context: QgsProcessingContext,
                         feedback: QgsProcessingFeedback) -> Dict[str, Any]:

        input_mesh_layer: QgsMeshLayer = self.parameterAsMeshLayer(
            parameters, self.INPUT, context)
        filename = filename_from_mesh_layer(input_mesh_layer)
        mesh = Mesh.open(filename)
        points_layer: QgsVectorLayer = self.parameterAsVectorLayer(
            parameters, self.POINTS, context)
        fields = QgsFields()
        fields.append(QgsField('Node_ID', type=QVariant.Int))
        sink, dest_id = self.parameterAsSink(
            parameters, self.OUTPUT, context, fields,
            geometryType=QgsWkbTypes.Type.Point, crs=points_layer.crs())

        # Extract points
        points: List[Point2D] = []
        feature: QgsFeature
        for feature in points_layer.getFeatures():
            for vertex in feature.geometry().vertices():
                points.append((vertex.x(), vertex.y()))

        # Resolve nodes
        if mesh.nodes:
            for point in points:
                node = mesh.get_node(point)
                new_feat = QgsFeature()
                new_feat.setGeometry(QgsPoint(*point))
                new_feat.setAttributes([node.id])
                sink.addFeature(new_feat)

        feedback.setProgress(100)
        return {self.OUTPUT: dest_id}


class GetMeshElementByLocation(BaseMeshProcessingAlgorithm):
    """Retrieve the IDs of the elements containing a set of points."""

    INPUT = 'INPUT'
    POINTS = 'POINTS'
    OUTPUT = 'OUTPUT'

    def displayName(self) -> str:
        return 'Get mesh element by location'

    def initAlgorithm(
            self, configuration: Optional[Dict[str, Any]] = None) -> None:

        _ = configuration
        # Input mesh
        self.addParameter(QgsProcessingParameterMeshLayer(
            name=self.INPUT,
            description='Mesh layer'))
        # Input points
        self.addParameter(QgsProcessingParameterVectorLayer(
            name=self.POINTS,
            description='Input points',
            types=[QgsProcessing.SourceType.TypeVectorPoint]))
        # Output
        self.addParameter(QgsProcessingParameterFeatureSink(
            name=self.OUTPUT,
            description='Output point layer',
            type=QgsProcessing.SourceType.TypeVectorPoint))

    def processAlgorithm(self, parameters: Dict[str, Any],
                         context: QgsProcessingContext,
                         feedback: QgsProcessingFeedback) -> Dict[str, Any]:

        input_mesh_layer: QgsMeshLayer = self.parameterAsMeshLayer(
            parameters, self.INPUT, context)
        filename = filename_from_mesh_layer(input_mesh_layer)
        mesh = Mesh.open(filename)
        points_layer: QgsVectorLayer = self.parameterAsVectorLayer(
            parameters, self.POINTS, context)
        fields = QgsFields()
        fields.append(QgsField('Element_ID', type=QVariant.Int))
        sink, dest_id = self.parameterAsSink(
            parameters, self.OUTPUT, context, fields,
            geometryType=QgsWkbTypes.Type.Point, crs=points_layer.crs())

        # Extract points
        points: List[Point2D] = []
        feature: QgsFeature
        for feature in points_layer.getFeatures():
            for vertex in feature.geometry().vertices():
                points.append((vertex.x(), vertex.y()))

        # Resolve elements
        if mesh.elements:
            for point in points:
                element = mesh.get_element(point)
                new_feat = QgsFeature()
                new_feat.setGeometry(QgsPoint(*point))
                new_feat.setAttributes([element.id])
                sink.addFeature(new_feat)

        feedback.setProgress(100)
        return {self.OUTPUT: dest_id}
