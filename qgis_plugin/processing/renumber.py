# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Processing algorithm for renumbering mesh nodes and elements."""

from typing import Any, Dict, Optional

from basemesh.utils import renumber_mesh
from qgis.core import (QgsProcessingParameterBoolean, QgsProcessingContext,
                       QgsProcessingFeedback, QgsProcessingParameterFile)

from ._base import BaseMeshProcessingAlgorithm

__all__ = [
    'RenumberMesh',
]


class RenumberMesh(BaseMeshProcessingAlgorithm):
    """Renumber the given mesh layer to be continuous and 1-indexed."""

    INPUT = 'INPUT'
    EXPORT_CONVERSION_TABLE = 'EXPORT_CONVERSION_TABLE'

    def displayName(self) -> str:
        return 'Renumber mesh'

    def initAlgorithm(
            self, configuration: Optional[Dict[str, Any]] = None) -> None:

        _ = configuration
        # Input file
        self.addParameter(QgsProcessingParameterFile(
            name=self.INPUT,
            description='Input mesh to renumber'))
        # Export conversion table
        self.addParameter(QgsProcessingParameterBoolean(
            name=self.EXPORT_CONVERSION_TABLE,
            description='Export conversion table',
            defaultValue=False))

    def processAlgorithm(self, parameters: Dict[str, Any],
                         context: QgsProcessingContext,
                         feedback: QgsProcessingFeedback) -> Dict[str, Any]:

        _ = feedback
        input_file = self.parameterAsFile(
            parameters, self.INPUT, context)
        log_id_changes = self.parameterAsBool(
            parameters, self.EXPORT_CONVERSION_TABLE, context)

        renumber_mesh(input_file, log_id_changes)

        return {}
