# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Base classes for BASEmesh procsesing algorithms."""

from typing import Any, Dict, Optional, TypeVar

from qgis.core import (QgsProcessingAlgorithm,
                       QgsProcessingContext, QgsProcessingFeedback)

__all__ = [
    'BaseMeshProcessingAlgorithm',
    'BaseMeshConverterAlgorithm',
]

_T = TypeVar('_T', bound='BaseMeshProcessingAlgorithm')


class BaseMeshProcessingAlgorithm(QgsProcessingAlgorithm):
    """Base class for BASEmesh procsesing algorithms."""

    def createInstance(self: _T) -> _T:
        """Return a new copy of the algorithm."""
        return type(self)()

    def name(self) -> str:
        """Return the unique algorithm name."""
        return self.__class__.__name__.lower()  # type: ignore

    def displayName(self) -> str:
        """Return the user-facing algorithm name."""
        raise NotImplementedError()

    def shortHelpString(self) -> str:
        """Return a short help string for the algorithm."""
        return _parse_docstring(self.__class__.__doc__)

    def initAlgorithm(
            self, configuration: Optional[Dict[str, Any]] = None) -> None:
        """Define the algorithm's inputs and outputs."""
        raise NotImplementedError()

    def processAlgorithm(
            self, parameters: Dict[str, Any],
            context: QgsProcessingContext,
            feedback: QgsProcessingFeedback) -> Dict[str, Any]:
        """Run the algorithm."""
        raise NotImplementedError()


class BaseMeshConverterAlgorithm(BaseMeshProcessingAlgorithm):
    """Sub-class for the "Converters" group of algorithms."""

    def name(self) -> str:
        """Return the unique algorithm name."""
        return f'{self.groupId()}{super().name()}'

    def group(self) -> str:
        """Return the localised name of the algorithm group."""
        return 'Converters'

    def groupId(self) -> str:
        """Return the unique group ID for the algorithm."""
        return 'converters'


def _parse_docstring(string: Optional[str]) -> str:
    """Convert a Python docstring for use as an algorithm help text.

    This ignores any leading and trailing whitespace and only keeps
    double linebreaks.

    Parameters
    ----------
    string : Optional[str]
        A docstring to parse into a QGIS help text

    Returns
    -------
    str
        The formatted docstring, or an empty string if no docstring was
        passed
    """
    out_string = ''
    if string is not None:
        for line in string.splitlines():
            line = line.strip()

            # Keep empty lines
            if line == '':
                out_string += '\n\n'
            # Any other lines get appended as normal
            else:
                out_string += f' {line}'
    return out_string
