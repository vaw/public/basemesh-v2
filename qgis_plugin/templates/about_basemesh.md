## BASEmesh

BASEmesh is a pre-processing toolkit for the numerical software BASEMENT and allows the creation and processing of 1D and 2D mesh geometries.

The core components of BASEmesh do not rely on the QGIS environment, which allows the creation of custom scripts and workflows through Python scripts.  
Additional information about the Python API can be found in the source code repository.

For more information, please visit <https://gitlab.ethz.ch/vaw/public/basemesh-v2/>.

## BASEMENT

BASEMENT is a numerical simulation software package developed at the Laboratory of Hydraulics, Hydrology and Glaciology (VAW) of the Swiss Federal Institute of Technology (ETH) in Zurich, Switzerland.

For more information, please visit <https://basement.ethz.ch/>.

## Triangle

The triangulation process of BASEmesh is based on Triangle, an excellent two-dimensional mesh generator developed by Jonathan Richard Shewchuk at the University of California at Berkeley.

For more information, please visit <https://www.cs.cmu.edu/~quake/triangle.html>.
