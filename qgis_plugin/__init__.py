# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""QGIS plugin component of BASEmesh."""

import pathlib
import sys
from typing import Any, Callable, Optional, Type, Union
import warnings

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMenu, QToolBar
from qgis.core import QgsApplication, Qgis
from qgis.gui import QgisInterface

# Set up basemesh directory
_libs_path = pathlib.Path(__file__).parent / 'libs'
sys.path.insert(5, str(_libs_path))
sys.path.insert(5, str(pathlib.Path(__file__).parent))


def _load_wheel(filename: str, prepend: bool = True) -> None:
    """Load a wheel from the packages directory into PYTHONPATH.

    Setting the `prepend` flag will move the wheel to the start of the
    list, making sure it is loaded instead of versions that may already
    be installed globally.
    """
    # Append file extension is not specified
    if not filename.endswith('.whl'):
        filename += '.whl'
    # Locate the wheel file
    wheel_file = _libs_path / filename
    if not wheel_file.exists():
        raise FileNotFoundError(f'Could not find wheel "{filename}"')
    # Add the wheel to path, making it available for import
    if prepend:
        sys.path.insert(0, str(wheel_file))
    else:
        sys.path.append(str(wheel_file))


# Set up third-party libraries
_load_wheel('cached_property-1.5.2-py2.py3-none-any')
_load_wheel('py2dm-0.2.2-py3-none-any')
_load_wheel('typing_extensions-3.7.4.3-py3-none-any')


try:
    import resources_rc  # type: ignore
    from .gui import (AboutDialog, ElevMeshDialog, InterpolDialog,
                      QualMeshDialog)
    from .processing import BasemeshProcessingProvider
except ImportError as err:
    raise ImportError(f'Unable to load BASEmesh plugin components: {err}')
try:
    from basemesh.errors import BasemeshWarning
except ImportError as err:
    raise ImportError(f'Unable to load BASEmesh module: {err}')

__all__ = [
    'BaseMeshPlugin',
    'classFactory'
]

__version__ = '2.2.0'

# This value is not used, but the import must be present for Qt to load the
# corresponding module.
# This statement is only here to signify that the import is needed but its
# value is not used.
_ = resources_rc  # type: ignore


class BaseMeshPlugin:
    """The BASEmesh plugin to be loaded into the QGIS application."""

    # pylint: disable=invalid-name

    display_name = 'BASEmesh 2'

    def __init__(self, iface: QgisInterface) -> None:
        """Prepare the plugin for initialisation.

        Note that this is called in preparation for the plugin being
        loaded. The class may be initialised, but the QGIS plugin side
        still requires the initGui() method to be called.

        Parameters
        ----------
        iface : QgisInterface
            The interface to generate the plugin's menu for
        """
        self.iface = iface
        self.menu: Optional[QMenu] = None
        self.toolbar: Optional[QToolBar] = None
        self.provider = BasemeshProcessingProvider()
        # Create actions
        self.about = QAction(
            QIcon(':/plugins/basemesh_v2/icons/basemesh_icon.svg'),
            'About BASEmesh')
        self.about.setStatusTip(
            'Additional information about BASEmesh, BASEMENT, and Triangle')
        self.about.triggered.connect(self._open_about)
        self.elevmesh = QAction(
            QIcon(':/plugins/basemesh_v2/icons/elev_meshing.svg'),
            'Elevation meshing')
        self.elevmesh.setStatusTip('Meshing of elevation data')
        self.elevmesh.triggered.connect(self._open_elevmesh)
        self.qualmesh = QAction(
            QIcon(':/plugins/basemesh_v2/icons/qual_meshing.svg'),
            'Quality meshing')
        self.qualmesh.setStatusTip('Meshing with quality constraints')
        self.qualmesh.triggered.connect(self._open_qualmesh)
        self.interpol = QAction(
            QIcon(':/plugins/basemesh_v2/icons/interpolation.svg'),
            'Interpolation')
        self.interpol.setStatusTip('Mesh interpolation')
        self.interpol.triggered.connect(self._open_interpol)

        # Set up warning forwarding
        self._old_showwarning: Optional[Callable[..., Any]] = None

    def initGui(self) -> None:
        """GUI initialization procedure.

        This method is called by QGIS to integrate the plugin with the
        QGIS user interface. This includes menu bar items and buttons.
        """
        # Create toolbar
        toolbar: QToolBar = self.iface.addToolBar(self.display_name)
        toolbar.setObjectName('basemeshToolBar')
        toolbar.addAction(self.elevmesh)
        toolbar.addAction(self.qualmesh)
        toolbar.addAction(self.interpol)
        self.toolbar = toolbar

        # Add plugin menu entry
        menu: QMenu = self.iface.pluginMenu().addMenu(
            QIcon(':/plugins/basemesh_v2/icons/basemesh_icon.svg'),
            self.display_name)
        menu.addAction(self.elevmesh)
        menu.addAction(self.qualmesh)
        menu.addAction(self.interpol)
        menu.addAction(self.about)
        self.menu = menu

        # Hook up processing algorithm provider
        QgsApplication.processingRegistry().addProvider(self.provider)

        self._old_showwarning = warnings.showwarning
        warnings.showwarning = self._forward_warning

    def _forward_warning(self, message: Union[Warning, str],
                         category: Type[Warning],
                         *args: Any, **kwargs: Any) -> None:
        if issubclass(category, BasemeshWarning):
            self.iface.messageBar().pushMessage(
                'BASEmesh warning', str(message),
                level=Qgis.MessageLevel.Warning)
        if self._old_showwarning is not None:
            self._old_showwarning(message, category, *args, **kwargs)

    def _open_about(self) -> None:
        AboutDialog(self.iface).exec_()

    def _open_elevmesh(self) -> None:
        ElevMeshDialog(self.iface).exec_()

    def _open_interpol(self) -> None:
        InterpolDialog(self.iface).exec_()

    def _open_qualmesh(self) -> None:
        QualMeshDialog(self.iface).exec_()

    def unload(self) -> None:
        """GUI breakdown procedure.

        This method is called when the plugin is unloaded. After this
        function completes, the interface is expected to be as if the
        plugin were never initialized in the first place.
        """
        # Destroy plugin menu entry
        if self.menu is not None:
            self.menu.removeAction(self.elevmesh)
            self.menu.removeAction(self.interpol)
            self.menu.removeAction(self.qualmesh)
            self.menu.removeAction(self.about)
            self.iface.pluginMenu().removeAction(self.menu.menuAction())

        # Remove processing algorithm provider
        QgsApplication.processingRegistry().removeProvider(self.provider)
        # Restore old warning handler
        warnings.showwarning = self._old_showwarning


def classFactory(iface: QgisInterface) -> 'BaseMeshPlugin':
    """Instantiate and return the BASEmesh plugin object.

    This function is called by QGIS  when loading the plugin and serves
    as the primary endpoint to the QGIS application.

    Parameters
    ----------
    iface : QgisInterface
        The QGIS interface to generate the plugin menu

    Returns
    -------
    BaseMeshPlugin
        The instantiated BASEmesh plugin to be used by QGIS
    """
    # pylint: disable=invalid-name
    return BaseMeshPlugin(iface)
