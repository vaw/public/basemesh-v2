# A component of the BASEmesh pre-processing toolkit.
# Copyright (C) 2020  ETH Zürich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Custom QgsFeedback subclass with status labels and busy state."""

import types
import typing

from PyQt5.QtCore import QObject, pyqtSignal


class _FeebackBusyContextManager:
    """Context manager for (un)setting the isBusy() flag."""

    def __init__(self, feedback: 'Feedback',
                 old_label: typing.Optional[str]) -> None:
        self._feedback = feedback
        self._old_label = old_label

    def __enter__(self) -> 'Feedback':
        self._feedback.setBusy(True)
        return self._feedback

    def __exit__(self, exc_type: typing.Optional[typing.Type[BaseException]],
                 exc_value: typing.Optional[BaseException],
                 traceback: typing.Optional[types.TracebackType],
                 ) -> bool:
        self._feedback.setBusy(False)
        if self._old_label is not None:
            self._feedback.setLabel(self._old_label)
        return False


class Feedback(QObject):
    """Custom QgsFeedback-like class supporting busy state and labels.

    This class does not support the `progressCount` property available
    in the original QgsFeedback class.

    To control the "busy" state, use the `setBusy` method. To get the
    current busy state, use either the `isBusy` getter or connect to
    the `busyChanged` signal.
    """

    busyChanged = pyqtSignal(bool)  # "isBusy" state changed
    canceled = pyqtSignal()  # "cancel" was called
    labelChanged = pyqtSignal(str)  # Status label changed
    progressChanged = pyqtSignal(float)  # Progress percentage changed
    statusChanged = pyqtSignal()  # Anything changed

    def __init__(self, parent: typing.Optional[QObject] = None,
                 status: str = '') -> None:
        super().__init__(parent)
        self._is_busy = False
        self._is_canceled = False
        self._label = status
        self._progress = 0.0
        self.busyChanged.connect(self.statusChanged.emit)
        self.labelChanged.connect(self.statusChanged.emit)
        self.progressChanged.connect(self.statusChanged.emit)  # type: ignore

    def cancel(self) -> None:
        """Cancel the feedback."""
        self._is_canceled = True
        self.canceled.emit()

    def isCanceled(self) -> bool:
        """Return whether the feedback was canceled."""
        return self._is_canceled

    def busy(self, label: typing.Optional[str] = None
             ) -> _FeebackBusyContextManager:
        """Context manager for automatically setting the `isBusy` flag.

        When entering the context manager, `isBusy` is set to True.
        Upon exiting it, it is set to False, regardless of its state
        before entering the context manager.
        """
        old_label: typing.Optional[str] = None
        if label is not None:
            old_label = self.label()
            self.setLabel(label)
        return _FeebackBusyContextManager(self, old_label)

    def isBusy(self) -> bool:
        """Return whether the feedback is reporting as busy."""
        return self._is_busy

    def setBusy(self, busy: bool) -> None:
        """Update the busy state.

        If the new value is different from the current value, the
        `busyChanged` signal will be emitted.
        """
        if busy != self._is_busy:
            self.busyChanged.emit(busy)
        self._is_busy = busy

    def label(self) -> str:
        """Return the current status label."""
        return self._label

    def setLabel(self, label: str) -> None:
        """Update the status label."""
        if label != self._label:
            self.labelChanged.emit(label)
        self._label = label

    def progress(self) -> float:
        """Return the current status progress."""
        return self._progress

    def setProgress(self, progress: float) -> None:
        """Update the status progress."""
        progress = self._clamp(progress)
        if progress != self._progress:
            self.progressChanged.emit(progress)
        self._progress = progress

    def _clamp(self, value: float) -> float:
        return max(0.0, min(100.0, value))


class MultiStepFeedback(Feedback):
    """Feedback wrapper for multi-step processes.

    The individual sub-steps are converted internally to produce a
    single, continuous progress.
    """

    def __init__(self, parent: typing.Optional[QObject] = None,
                 status: str = '') -> None:
        super().__init__(parent, status)
        self._step_count = 1
        self._step = 1

    def label(self) -> str:
        """Return the current status label.

        This variant also includes the current step number.
        """
        if self._step_count <= 0:
            return super().label()
        string = f'Step {self._step} of {self._step_count}'
        if self._label:
            string += f': {self._label}'
        return string

    def stepCount(self) -> int:
        """Return the total number of steps."""
        return self._step_count

    def setStepCount(self, steps: int) -> None:
        """Set the total number of steps.
        
        Setting step count to 0 will disable step count display.
        """
        if steps < 0:
            steps = 0
        self._step_count = steps
        if self._step > steps:
            self._step = steps

    def step(self) -> int:
        """Return the current step number. (1-based)"""
        return self._step

    def setStep(self, step: int) -> None:
        """Set the current step number. (1-based)"""
        if step < 1:
            step = 1
        if step > self._step_count:
            step = self._step_count
        self._step = step
        self.setProgress(0.0)

    def setProgress(self, progress: float) -> None:
        if self._step_count > 0:
            progress = self._scale(progress)
        super().setProgress(progress)

    def _calculateStepParameters(self) -> typing.Tuple[float, float]:
        """Calculate the scaling factor and offset for the progress."""
        if self._step_count <= 1:
            return 1.0, 0.0
        else:
            return 1.0 / self._step_count, (self._step - 1) / self._step_count

    def _scale(self, value: float) -> float:
        scaling, offset = self._calculateStepParameters()
        return value * scaling + offset * 100.0
